package com.example.myhotel;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.annotation.Documented;
import java.util.ArrayList;

public class TemplatePDF {

    private Context context;
    private File pdfFile;
    private Document document;
    private PdfWriter pdfWriter;
    private Paragraph paragraph;
    private Font fTitle=new Font(Font.FontFamily.TIMES_ROMAN,20,Font.BOLD);
    private Font fSubTitle=new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD);
    private Font fText=new Font(Font.FontFamily.TIMES_ROMAN,12,Font.BOLD);
    private Font fHighText=new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD, new BaseColor(25, 118, 210));

    public TemplatePDF(Context context) {
        this.context=context;
    }

    public  void openDocument(){
        createFile();
        try {
            document=new Document(PageSize.A4);
            pdfWriter=PdfWriter.getInstance(document,new FileOutputStream(pdfFile));
            document.open();
        }catch (Exception e){
            Log.e("openDocument",e.toString());
        }
    }

    private void createFile(){
        File folder=new File(Environment.getExternalStorageDirectory().toString(),"PDF");

        if (!folder.exists())
            folder.mkdir();
        pdfFile=new File(folder,"TemplatePDF.pdf");

    }

    public void closeDocument(){
        document.close();
    }

    public void addMetadaData(String title,String subject,String author){
        document.addTitle(title);
        document.addSubject(subject);
        document.addAuthor(author);
    }

    public void addTitles(String title, String subTitle,String subTitle2,String date){
        try {
            paragraph=new Paragraph();
            addChildP(new Paragraph(title,fTitle));
            addChildP(new Paragraph(subTitle,fSubTitle));
            addChildP(new Paragraph(subTitle2,fSubTitle));
            addChildP(new Paragraph("Generado: "+date,fHighText));
            paragraph.setSpacingAfter(30);
            document.add(paragraph);
        }catch (Exception e){
            Log.e("AddTitles",e.toString());
        }
    }

    public void addSubTitles(String subTitle){
        try {
            paragraph=new Paragraph();
            addChildH(new Paragraph(subTitle,fTitle));
            document.add(paragraph);
        }catch (Exception e){
            Log.e("addSubTitles",e.toString());
        }
    }

    public void addTotal(String subTitle,String subTitle2,String subTitle3){
        try {
            paragraph=new Paragraph();
            addChildF(new Paragraph(subTitle,fSubTitle));
            addChildF(new Paragraph(subTitle2,fSubTitle));
            addChildF(new Paragraph(subTitle3,fSubTitle));
            document.add(paragraph);
        }catch (Exception e){
            Log.e("addTotal",e.toString());
        }
    }

    private void addChildH(Paragraph chilldParagraph){
        paragraph.add(chilldParagraph);
    }

    private void addChildP(Paragraph chilldParagraph){
        chilldParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(chilldParagraph);
    }

    private void addChildF(Paragraph chilldParagraph){
        chilldParagraph.setAlignment(Element.ALIGN_RIGHT);
        paragraph.add(chilldParagraph);
    }

    public void addParagraph(String text){
        try {
            paragraph= new Paragraph(text,fText);
            paragraph.setSpacingAfter(5);
            paragraph.setSpacingBefore(5);
            document.add(paragraph);
        }catch (Exception e){
            Log.e("addParagraph",e.toString());
        }
    }
    public void createTable(String[] header, ArrayList<String[]>bill){
        try {
            paragraph=new Paragraph();
            paragraph.setFont(fText);
            PdfPTable pdfPTable=new PdfPTable(header.length);
            pdfPTable.setWidthPercentage(100);
            pdfPTable.setSpacingBefore(20);
            PdfPCell pdfPCell;
            int indexC=0;
            while (indexC<header.length){
                pdfPCell=new PdfPCell(new Phrase(header[indexC++],fSubTitle));
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setBackgroundColor(new BaseColor(25, 118, 210));
                pdfPCell.setPadding(5);
                pdfPTable.addCell(pdfPCell);
            }

            for (int indexR=0;indexR<bill.size();indexR++){
                String[] row=bill.get(indexR);
                for (indexC=0;indexC<header.length;indexC++){
                    pdfPCell=new PdfPCell(new Phrase(row[indexC]));
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setPadding(7);
                    pdfPTable.addCell(pdfPCell);
                }
            }
            paragraph.add(pdfPTable);
            document.add(paragraph);
        }catch (Exception e){
            Log.e("createTable",e.toString());
        }
    }
    public void viewPDF(Activity activity){
        /*Intent intent=new Intent(context,ViewPDFActivity.class);
        intent.putExtra("path",pdfFile.getAbsolutePath());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);*/
        if (pdfFile.exists()){
            Uri uri= FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", pdfFile);
            Intent intent=new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri,"application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                activity.startActivity(intent);
            }catch (ActivityNotFoundException e){
                activity.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=com.adobe.reader")));
                Toast.makeText(activity.getApplicationContext(),"No cuentas con una aplication para visualizar PDF",Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(activity.getApplicationContext(),"No se encontró el archivo.",Toast.LENGTH_LONG).show();
        }
    }

}
