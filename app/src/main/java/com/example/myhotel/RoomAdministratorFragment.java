package com.example.myhotel;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RoomAdministratorFragment extends Fragment {

    public RoomAdministratorFragment() {
        // Required empty public constructor
    }

    public static RoomAdministratorFragment newInstance(String param1, String param2) {
        RoomAdministratorFragment fragment = new RoomAdministratorFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_room_administrator, container, false);
    }
}