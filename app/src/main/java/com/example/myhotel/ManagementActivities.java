package com.example.myhotel;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


public class ManagementActivities {

    public static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public static void logout(Activity activity,int myLayout, String description) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(activity);
        View layoutView=activity.getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor user = activity.getSharedPreferences("user", Context.MODE_PRIVATE).edit();
                user.clear().apply();
                user.commit();

                finishActivity(activity,MainActivity.class);

                System.exit(0);
            }
        });

    }

    public static void restartActivity(Activity activity){

        //finalizamos la actividad actual
        activity.finish();

        //llamamos a la actividad
        activity.startActivity(activity.getIntent());
    }

    public static void redirectActivity(Activity activity, Class aClass) {
        Intent intent=new Intent(activity,aClass);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        activity.startActivity(intent);
    }

    public static void finishActivity(Activity activity, Class aClass) {
        Intent intent=new Intent(activity,aClass);

        activity.startActivity(intent);

        activity.finish();
    }

    public static void showAlertDialog(Activity activity,int myLayout, String description) {
        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(activity);
        View layoutView=activity.getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.buttonOk);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

}
