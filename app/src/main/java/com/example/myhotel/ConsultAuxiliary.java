package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConsultAuxiliary extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName,txtConsultAuxiliary,tvNombre,tvCorreo,txtSeeMore;
    TableLayout tableLayout;
    RequestQueue requestQueue;
    ArrayList<UserArrayList> arrayList=new ArrayList<>();
    TableRow fila;
    Button btnSeeMore;
    ImageView img;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_auxiliary);

        InitializeElements();
        ConsultDataAuxiliary();
        NavLoadData();
        NavFragmentData();

    }

    public void ConsultDataAuxiliary(){

        ProgressDialog pDialog = new ProgressDialog(ConsultAuxiliary.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("totalAux");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("usuarios").toString());
                                final int total = jsonArray.length();

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    if (jsonObject.get("rol").toString().equals("AUX")){

                                        String[] nombre=jsonObject.get("nombre").toString().split(" ");
                                        String[] apellido=jsonObject.get("apellido").toString().split(" ");

                                        arrayList.add(new UserArrayList(jsonObject.get("identificacion").toString(),
                                                nombre[0],apellido[0],
                                                jsonObject.get("correo").toString(),jsonObject.get("telefono").toString(),
                                                jsonObject.get("estado").toString(),jsonObject.get("rol").toString(),
                                                jsonObject.get("img").toString(),jsonObject.get("uid").toString()));

                                    }
                                }

                                generateTable(arrayList);

                            }else{

                                txtConsultAuxiliary.setTextSize(18);
                                txtConsultAuxiliary.setGravity(Gravity.CENTER);
                                txtConsultAuxiliary.setText("No se encontro información de auxiliar.");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public void generateTable(ArrayList<UserArrayList> arrayList){
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,45);
        TableRow.LayoutParams layoutEmail = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,45);
        TableRow.LayoutParams layoutSeeMore = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,10);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(ConsultAuxiliary.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvNombre = new TextView(ConsultAuxiliary.this);
                tvNombre.setText("NOMBRE");
                tvNombre.setTextSize(18);
                tvNombre.setGravity(Gravity.CENTER);
                tvNombre.setBackgroundColor(Color.parseColor("#1976D2"));
                tvNombre.setTextColor(Color.WHITE);
                tvNombre.setPadding(10, 10, 10, 10);
                tvNombre.setLayoutParams(layoutName);
                fila.addView(tvNombre);

                tvCorreo = new TextView(ConsultAuxiliary.this);
                tvCorreo.setText("CORREO");
                tvCorreo.setTextSize(18);
                tvCorreo.setGravity(Gravity.CENTER);
                tvCorreo.setBackgroundColor(Color.parseColor("#1976D2"));
                tvCorreo.setTextColor(Color.WHITE);
                tvCorreo.setPadding(10, 10, 10, 10);
                tvCorreo.setLayoutParams(layoutName);
                fila.addView(tvCorreo);

                txtSeeMore = new TextView(ConsultAuxiliary.this);
                txtSeeMore.setTextSize(18);
                txtSeeMore.setGravity(Gravity.CENTER);
                txtSeeMore.setBackgroundColor(Color.parseColor("#1976D2"));
                txtSeeMore.setTextColor(Color.WHITE);
                txtSeeMore.setPadding(10, 10, 10, 10);
                txtSeeMore.setLayoutParams(layoutName);
                fila.addView(txtSeeMore);

                tableLayout.addView(fila);
            } else {
                tvNombre = new TextView(ConsultAuxiliary.this);
                tvNombre.setTextSize(16);
                tvNombre.setGravity(Gravity.CENTER);
                tvNombre.setText(arrayList.get(i).getNombre()+" "+arrayList.get(i).getApellido());
                tvNombre.setPadding(10, 10, 10, 10);
                tvNombre.setHorizontallyScrolling(true);
                tvNombre.setEllipsize(TextUtils.TruncateAt.END);
                tvNombre.setMaxLines(1);
                tvNombre.setLayoutParams(layoutName);
                fila.addView(tvNombre);

                tvCorreo = new TextView(ConsultAuxiliary.this);
                tvCorreo.setTextSize(16);
                tvCorreo.setGravity(Gravity.CENTER);
                tvCorreo.setText(arrayList.get(i).getCorreo());
                tvCorreo.setPadding(10, 10, 10, 10);
                tvCorreo.setHorizontallyScrolling(true);
                tvCorreo.setEllipsize(TextUtils.TruncateAt.END);
                tvCorreo.setMaxLines(1);
                tvCorreo.setLayoutParams(layoutEmail);
                fila.addView(tvCorreo);

                btnSeeMore = new Button(ConsultAuxiliary.this);
                btnSeeMore.setGravity(Gravity.CENTER);
                btnSeeMore.setBackgroundColor(Color.TRANSPARENT);
                btnSeeMore.setText("Ver mas...");
                btnSeeMore.setTextColor(Color.parseColor("#1976D2"));
                int finalI = i;
                btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ConsultAuxiliary.this, MoreInfoAuxiliary.class);
                        intent.putExtra("uid", arrayList.get(finalI).getUid());
                        startActivity(intent);
                    }
                });
                btnSeeMore.setPadding(10, 10, 10, 10);
                btnSeeMore.setLayoutParams(layoutSeeMore);
                fila.addView(btnSeeMore);

                tableLayout.addView(fila);

            }
        }
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickConsultAuxiliary(View view){
        recreate();
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(ConsultAuxiliary.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        tableLayout=(TableLayout)findViewById(R.id.tableLayout);
        txtConsultAuxiliary=(TextView)findViewById(R.id.txtConsultAuxiliary);
        requestQueue= Volley.newRequestQueue(this);
        img=(ImageView) findViewById(R.id.img);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}