package com.example.myhotel;

public class ReportRoomTypeArrayList {
    public String categoria;
    public  String total;

    public ReportRoomTypeArrayList(String categoria, String total) {
        this.categoria = categoria;
        this.total = total;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
