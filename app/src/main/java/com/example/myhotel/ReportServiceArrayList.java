package com.example.myhotel;

public class ReportServiceArrayList {
    public String nombre;
    public String total;

    public ReportServiceArrayList(String nombre, String total) {
        this.nombre = nombre;
        this.total = total;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
