package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SessionAdministrator extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName,tvNombreService,tvTotalService;
    ImageView img;
    ArrayList<ReportServiceArrayList> arrayList=new ArrayList<>();
    ArrayList<ReportRoomTypeArrayList> arrayList2=new ArrayList<>();
    ArrayList<ReportClientArrayList> arrayList3=new ArrayList<>();
    RequestQueue requestQueue;
    TableRow fila;
    TableLayout tableLayout,tableLayout2,tableLayout3;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/reporte/servicio";
    private static final String URL2="https://udmyhotelproject.herokuapp.com/myhotel/reporte/tipo_habitacion";
    private static final String URL3="https://udmyhotelproject.herokuapp.com/myhotel/reporte/cliente_frecuente";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_administrator);

        InitializeElements();
        ConsultReportService();
        ConsultReportRoomType();
        ConsultReportClient();
        NavLoadData();
        NavFragmentData();

    }

    public void ConsultReportClient(){

        ProgressDialog pDialog = new ProgressDialog(SessionAdministrator.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL3,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            pDialog.dismiss();

                            JSONArray jsonArray= new JSONArray(response.get("topClienteFrecuente").toString());
                            final int total = jsonArray.length();

                            if (total>=1){

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());

                                    arrayList3.add(new ReportClientArrayList(jsonObject.get("nombre").toString(),jsonObject.get("total_visitas").toString() ));

                                }

                                generateReportClient(arrayList3);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void generateReportClient(ArrayList<ReportClientArrayList> arrayList) {
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,50);
        TableRow.LayoutParams layoutEmail = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,50);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(SessionAdministrator.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvNombreService = new TextView(SessionAdministrator.this);
                tvNombreService.setText("Nombre");
                tvNombreService.setTextSize(18);
                tvNombreService.setGravity(Gravity.CENTER);
                tvNombreService.setBackgroundColor(Color.parseColor("#1976D2"));
                tvNombreService.setTextColor(Color.WHITE);
                tvNombreService.setPadding(10, 10, 10, 10);
                tvNombreService.setLayoutParams(layoutName);
                fila.addView(tvNombreService);

                tvTotalService = new TextView(SessionAdministrator.this);
                tvTotalService.setText("TOTAL VISITAS");
                tvTotalService.setTextSize(18);
                tvTotalService.setGravity(Gravity.CENTER);
                tvTotalService.setBackgroundColor(Color.parseColor("#1976D2"));
                tvTotalService.setTextColor(Color.WHITE);
                tvTotalService.setPadding(10, 10, 10, 10);
                tvTotalService.setLayoutParams(layoutName);
                fila.addView(tvTotalService);

                tableLayout3.addView(fila);
            } else {
                tvNombreService = new TextView(SessionAdministrator.this);
                tvNombreService.setTextSize(16);
                tvNombreService.setGravity(Gravity.CENTER);
                tvNombreService.setText(arrayList.get(i).getNombre());
                tvNombreService.setPadding(10, 10, 10, 10);
                tvNombreService.setHorizontallyScrolling(true);
                tvNombreService.setEllipsize(TextUtils.TruncateAt.END);
                tvNombreService.setMaxLines(1);
                tvNombreService.setLayoutParams(layoutName);
                fila.addView(tvNombreService);

                tvTotalService = new TextView(SessionAdministrator.this);
                tvTotalService.setTextSize(16);
                tvTotalService.setGravity(Gravity.CENTER);
                tvTotalService.setText(arrayList.get(i).getTotal());
                tvTotalService.setPadding(10, 10, 10, 10);
                tvTotalService.setHorizontallyScrolling(true);
                tvTotalService.setEllipsize(TextUtils.TruncateAt.END);
                tvTotalService.setMaxLines(1);
                tvTotalService.setLayoutParams(layoutEmail);
                fila.addView(tvTotalService);

                tableLayout3.addView(fila);

            }
        }
    }

    public void ConsultReportService(){

        ProgressDialog pDialog = new ProgressDialog(SessionAdministrator.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            pDialog.dismiss();

                            JSONArray jsonArray= new JSONArray(response.get("topServicios").toString());
                            final int total = jsonArray.length();

                            if (total>=1){

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());

                                        arrayList.add(new ReportServiceArrayList(jsonObject.get("nombre").toString(),jsonObject.get("total").toString() ));

                                }

                                generateReportService(arrayList);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public void ConsultReportRoomType(){

        ProgressDialog pDialog = new ProgressDialog(SessionAdministrator.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            pDialog.dismiss();

                            JSONArray jsonArray= new JSONArray(response.get("topTipoHabitacion").toString());
                            final int total = jsonArray.length();

                            if (total>=1){

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());

                                    arrayList2.add(new ReportRoomTypeArrayList(jsonObject.get("categoria").toString(),jsonObject.get("total").toString() ));

                                }

                                generateReportRoomType(arrayList2);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void generateReportRoomType(ArrayList<ReportRoomTypeArrayList> arrayList) {
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,50);
        TableRow.LayoutParams layoutEmail = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,50);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(SessionAdministrator.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvNombreService = new TextView(SessionAdministrator.this);
                tvNombreService.setText("CATEGORIA");
                tvNombreService.setTextSize(18);
                tvNombreService.setGravity(Gravity.CENTER);
                tvNombreService.setBackgroundColor(Color.parseColor("#1976D2"));
                tvNombreService.setTextColor(Color.WHITE);
                tvNombreService.setPadding(10, 10, 10, 10);
                tvNombreService.setLayoutParams(layoutName);
                fila.addView(tvNombreService);

                tvTotalService = new TextView(SessionAdministrator.this);
                tvTotalService.setText("TOTAL");
                tvTotalService.setTextSize(18);
                tvTotalService.setGravity(Gravity.CENTER);
                tvTotalService.setBackgroundColor(Color.parseColor("#1976D2"));
                tvTotalService.setTextColor(Color.WHITE);
                tvTotalService.setPadding(10, 10, 10, 10);
                tvTotalService.setLayoutParams(layoutName);
                fila.addView(tvTotalService);

                tableLayout2.addView(fila);
            } else {
                tvNombreService = new TextView(SessionAdministrator.this);
                tvNombreService.setTextSize(16);
                tvNombreService.setGravity(Gravity.CENTER);
                tvNombreService.setText(arrayList.get(i).getCategoria());
                tvNombreService.setPadding(10, 10, 10, 10);
                tvNombreService.setHorizontallyScrolling(true);
                tvNombreService.setEllipsize(TextUtils.TruncateAt.END);
                tvNombreService.setMaxLines(1);
                tvNombreService.setLayoutParams(layoutName);
                fila.addView(tvNombreService);

                tvTotalService = new TextView(SessionAdministrator.this);
                tvTotalService.setTextSize(16);
                tvTotalService.setGravity(Gravity.CENTER);
                tvTotalService.setText(arrayList.get(i).getTotal());
                tvTotalService.setPadding(10, 10, 10, 10);
                tvTotalService.setHorizontallyScrolling(true);
                tvTotalService.setEllipsize(TextUtils.TruncateAt.END);
                tvTotalService.setMaxLines(1);
                tvTotalService.setLayoutParams(layoutEmail);
                fila.addView(tvTotalService);

                tableLayout2.addView(fila);

            }
        }
    }

    private void generateReportService(ArrayList<ReportServiceArrayList> arrayList) {
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,50);
        TableRow.LayoutParams layoutEmail = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,50);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(SessionAdministrator.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvNombreService = new TextView(SessionAdministrator.this);
                tvNombreService.setText("NOMBRE");
                tvNombreService.setTextSize(18);
                tvNombreService.setGravity(Gravity.CENTER);
                tvNombreService.setBackgroundColor(Color.parseColor("#1976D2"));
                tvNombreService.setTextColor(Color.WHITE);
                tvNombreService.setPadding(10, 10, 10, 10);
                tvNombreService.setLayoutParams(layoutName);
                fila.addView(tvNombreService);

                tvTotalService = new TextView(SessionAdministrator.this);
                tvTotalService.setText("TOTAL");
                tvTotalService.setTextSize(18);
                tvTotalService.setGravity(Gravity.CENTER);
                tvTotalService.setBackgroundColor(Color.parseColor("#1976D2"));
                tvTotalService.setTextColor(Color.WHITE);
                tvTotalService.setPadding(10, 10, 10, 10);
                tvTotalService.setLayoutParams(layoutName);
                fila.addView(tvTotalService);

                tableLayout.addView(fila);
            } else {
                tvNombreService = new TextView(SessionAdministrator.this);
                tvNombreService.setTextSize(16);
                tvNombreService.setGravity(Gravity.CENTER);
                tvNombreService.setText(arrayList.get(i).getNombre());
                tvNombreService.setPadding(10, 10, 10, 10);
                tvNombreService.setHorizontallyScrolling(true);
                tvNombreService.setEllipsize(TextUtils.TruncateAt.END);
                tvNombreService.setMaxLines(1);
                tvNombreService.setLayoutParams(layoutName);
                fila.addView(tvNombreService);

                tvTotalService = new TextView(SessionAdministrator.this);
                tvTotalService.setTextSize(16);
                tvTotalService.setGravity(Gravity.CENTER);
                tvTotalService.setText(arrayList.get(i).getTotal());
                tvTotalService.setPadding(10, 10, 10, 10);
                tvTotalService.setHorizontallyScrolling(true);
                tvTotalService.setEllipsize(TextUtils.TruncateAt.END);
                tvTotalService.setMaxLines(1);
                tvTotalService.setLayoutParams(layoutEmail);
                fila.addView(tvTotalService);

                tableLayout.addView(fila);

            }
        }
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ClickHomeAdministrator(View view){
        recreate();
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }

    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(SessionAdministrator.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
    }

    public void NavLoadData(){

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        requestQueue= Volley.newRequestQueue(this);
        tableLayout=(TableLayout)findViewById(R.id.tableLayout);
        tableLayout2=(TableLayout)findViewById(R.id.tableLayout2);
        tableLayout3=(TableLayout)findViewById(R.id.tableLayout3);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }
}