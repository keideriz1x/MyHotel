package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class CreateRoom extends AppCompatActivity{

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName;
    ImageView img;
    Button btnCreateRoom;
    Spinner spRoomType;
    EditText edtCreateRoom;
    ArrayList<RoomTypeArrayList> arrayList=new ArrayList<>();
    RequestQueue requestQueue;
    String tipo_habitacion;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/tipoH/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);

        InitializeElements();
        ConsultDataRoomType();

        btnCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number=edtCreateRoom.getText().toString();

                Create(number,tipo_habitacion);

            }
        });
        NavLoadData();
        NavFragmentData();

    }

    public void Create(String number, String tipo_habitacion) {

        JSONObject jsonParams = new JSONObject();

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/habitacion/";
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if ((!number.isEmpty()) && (!tipo_habitacion.isEmpty())){

            ProgressDialog pDialog = new ProgressDialog(CreateRoom.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("numero", Integer.parseInt(number));
                jsonParams.put("tipo_habitacion", tipo_habitacion);

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.POST,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();


                            alert(R.layout.my_success_dialog,"¡Habitación creada exitosamente!");

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            ){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(CreateRoom.this,myLayout,description);
    }

    public void ConsultDataRoomType(){

        ProgressDialog pDialog = new ProgressDialog(CreateRoom.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("total");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("habitacion").toString());
                                final int total = jsonArray.length();

                                arrayList.add(new RoomTypeArrayList("Seleccione tipo de habitación", "0", "false",
                                        "","", 0));

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    arrayList.add(new RoomTypeArrayList(jsonObject.get("categoria").toString(),
                                            jsonObject.get("camas").toString(), jsonObject.get("terraza").toString(),
                                            jsonObject.get("img").toString(),jsonObject.get("uid").toString(),
                                            (Integer) jsonObject.get("precio")));
                                }

                                generateList(arrayList);

                            }else{



                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void generateList(ArrayList<RoomTypeArrayList> arrayList) {
        ArrayAdapter<RoomTypeArrayList> arrayAdapter=new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,arrayList);

        spRoomType.setAdapter(arrayAdapter);

        spRoomType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position !=0){
                    RoomTypeArrayList roomTypeArrayList=(RoomTypeArrayList) parent.getItemAtPosition(position);
                    tipo_habitacion=roomTypeArrayList.getUid();
                }else{
                    tipo_habitacion="";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickCreateRoom(View view){
        recreate();
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(CreateRoom.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        btnCreateRoom=(Button) findViewById(R.id.btnCreateRoom);
        spRoomType=(Spinner) findViewById(R.id.spRoomType);
        edtCreateRoom=(EditText) findViewById(R.id.edtCreateRoom);
        requestQueue= Volley.newRequestQueue(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}