package com.example.myhotel;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ServiceAuxiliaryFragment extends Fragment {

    public ServiceAuxiliaryFragment() {
        // Required empty public constructor
    }

    public static ServiceAuxiliaryFragment newInstance(String param1, String param2) {
        ServiceAuxiliaryFragment fragment = new ServiceAuxiliaryFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service_auxiliary, container, false);
    }
}