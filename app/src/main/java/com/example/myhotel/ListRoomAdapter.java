package com.example.myhotel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ListRoomAdapter extends RecyclerView.Adapter<ListRoomAdapter.RoomHolder> {

    ArrayList<RoomTypeArrayList> listRoom;


    public ListRoomAdapter(ArrayList<RoomTypeArrayList> listRoom) {
        this.listRoom = listRoom;
    }

    @NonNull
    @Override
    public RoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_room,null,false);
        return new RoomHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.vTitle.setText(listRoom.get(position).getCategoria());
        if (listRoom.get(position).getTerraza().equals("true")){
            holder.vTerrace.setText("Terraza: Si");
        }else {
            holder.vTerrace.setText("Terraza: No");
        }
        holder.vBed.setText("Camas "+listRoom.get(position).getCamas());

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String price = formatter.format(listRoom.get(position).getPrecio());

        holder.vPrice.setText("COP "+price);

        if (!listRoom.get(position).getImg().equals("")){
            Picasso.get()
                    .load(listRoom.get(position).getImg())
                    .into(holder.vImage);
        }else{
            Picasso.get()
                    .load(R.drawable.defecto)
                    .into(holder.vImage);
        }

        holder.vReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Create(listRoom.get(position).getUid() ,v.getContext());
            }
        });

    }

    private void Create(String uid, Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        AlertDialog alertDialog;

        LinearLayout vertical=new LinearLayout(context);
        vertical.setOrientation(LinearLayout.VERTICAL);
        vertical.setPadding(50,30,50,50);

        TextView txtEditTitle=new TextView(context);
        txtEditTitle.setText("Crear Reservación");
        txtEditTitle.setTextSize(27);
        txtEditTitle.setGravity(Gravity.CENTER);
        txtEditTitle.setTypeface(null, Typeface.BOLD);
        txtEditTitle.setPadding(20,20,20,20);
        vertical.addView(txtEditTitle);

        TextInputLayout fInicioTextInputLayout = new TextInputLayout(context);
        TextInputEditText edtFinicio = new TextInputEditText(fInicioTextInputLayout.getContext());
        fInicioTextInputLayout.setHint("Fecha de Llegada");
        fInicioTextInputLayout.addView(edtFinicio);
        fInicioTextInputLayout.setPadding(0,20,0,20);
        edtFinicio.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                Calendar c=Calendar.getInstance();
                int year=c.get(Calendar.YEAR);
                int month=c.get(Calendar.MONTH);
                int day=c.get(Calendar.DAY_OF_MONTH);

                if(hasFocus) {

                    edtFinicio.setInputType(InputType.TYPE_NULL);

                    DatePickerDialog dpd= new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                            edtFinicio.setText( mYear + "-" + (mMonth+1) + "-" + mDay);
                            edtFinicio.clearFocus();
                        }
                    },year,month,day);

                    dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                    dpd.show();
                } else {

                    edtFinicio.setInputType(InputType.TYPE_NULL);
                    edtFinicio.clearFocus();

                }
            }
        });

        vertical.addView(fInicioTextInputLayout);

        TextInputLayout fFinTextInputLayout = new TextInputLayout(context);
        TextInputEditText edtFFin = new TextInputEditText(fFinTextInputLayout.getContext());
        fFinTextInputLayout.setHint("Fecha de Salida");
        fFinTextInputLayout.addView(edtFFin);
        fFinTextInputLayout.setPadding(0,20,0,20);

        edtFFin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    Calendar c=Calendar.getInstance();
                    int year=c.get(Calendar.YEAR);
                    int month=c.get(Calendar.MONTH);
                    int day=c.get(Calendar.DAY_OF_MONTH);

                    edtFFin.setInputType(InputType.TYPE_NULL);

                    DatePickerDialog dpd= new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                            edtFFin.setText( mYear + "-" + (mMonth+1) + "-" + mDay);
                            edtFFin.clearFocus();
                        }
                    },year,month,day);

                    dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                    dpd.show();
                }else{
                    edtFFin.setInputType(InputType.TYPE_NULL);
                    edtFFin.clearFocus();
                }
            }
        });

        vertical.addView(fFinTextInputLayout);

        View viewDivider = new View(context);
        int dividerHeight = (int) (context.getResources().getDisplayMetrics().density * 20); // 1dp to pixels
        viewDivider.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider);

        AppCompatButton btnEditCancel=new AppCompatButton(context);
        btnEditCancel.setText("Cancelar");
        btnEditCancel.layout(50,20,50,20);
        btnEditCancel.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditCancel);

        View viewDivider2 = new View(context);
        viewDivider2.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider2);

        Button btnEditAccept=new Button(context);
        btnEditAccept.setText("Aceptar");
        btnEditAccept.setPadding(50,20,50,20);
        btnEditAccept.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditAccept);

        dialog.setView(vertical);
        alertDialog=dialog.create();
        alertDialog.show();

        btnEditCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnEditAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                CreateReservation(edtFinicio.getText().toString(),edtFFin.getText().toString(),uid,context);
            }
        });
    }

    private void CreateReservation(String fInicio, String FFin, String uid, Context context) {
        JSONObject jsonParams = new JSONObject();

        RequestQueue requestQueue= Volley.newRequestQueue(context);

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/reserva/";
        SharedPreferences user = context.getSharedPreferences("user", Context.MODE_PRIVATE);

        if ((!fInicio.isEmpty()) && (!FFin.isEmpty())){

            ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(context.getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("fecha_inicio", fInicio);
                jsonParams.put("fecha_fin", FFin);
                jsonParams.put("tipo_habitacion", uid);

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.POST,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();


                            alert(R.layout.my_success_dialog,"¡Reserva creada exitosamente!",context);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString(),context);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            ){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.",context);
        }
    }

    private void alert(int myLayout, String description, Context context) {
        ManagementActivities.showAlertDialog((Activity) context,myLayout,description);
    }

    @Override
    public int getItemCount() {
        return listRoom.size();
    }

    public class RoomHolder extends RecyclerView.ViewHolder {

        ImageView vImage;
        TextView vTitle,vBed,vTerrace,vPrice;
        Button vReservation;

        public RoomHolder(@NonNull View itemView) {
            super(itemView);

            vImage=itemView.findViewById(R.id.vImage);
            vTitle=itemView.findViewById(R.id.vTitle);
            vBed=itemView.findViewById(R.id.vBed);
            vTerrace=itemView.findViewById(R.id.vTerrace);
            vPrice=itemView.findViewById(R.id.vPrice);
            vReservation=itemView.findViewById(R.id.vReservation);

        }
    }
}
