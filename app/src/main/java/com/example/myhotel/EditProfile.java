package com.example.myhotel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class EditProfile extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavClientFragment,NavAdministratorFragment,NavAuxiliaryFragment;
    TextView txtName;
    ImageView img;
    EditText edtEditNameProfile,edtEditLastNameProfile,edtEditEmailProfile,edtEditPhoneProfile,edtEditIdProfile,edtEditPassword,edtEditPassword2;
    Button btnEditImgProfile,btnEditProfile,btnEditPassword;
    ImageView EditImgProfile,image;
    RequestQueue requestQueue;
    String currentPhotoPath;
    Uri imageServer;
    boolean isCamera = false;
    boolean isGallery = false;
    AppCompatButton btnEditImageCancel;
    Button btnEditImageAccept;
    Bitmap bitmap;
    private static final int COD_SELECCIONAR = 10;
    private static final int COD_FOTO = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        InitializeElements();

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);
        String uid=user.getString("uid","");

        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Name=edtEditNameProfile.getText().toString();
                String LastName=edtEditLastNameProfile.getText().toString();
                String Email=edtEditEmailProfile.getText().toString();
                String Phone=edtEditPhoneProfile.getText().toString();
                String Id=edtEditIdProfile.getText().toString();

                EditDataProfile(Name,LastName,Email,Phone,Id,uid);

            }
        });

        btnEditImgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String img=user.getString("img","");
                String uid=user.getString("uid","");

                EditDataImgProfile(img,uid);

            }
        });

        btnEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clave=edtEditPassword.getText().toString();
                String clave2=edtEditPassword2.getText().toString();

                EditDataPassword(clave,clave2,uid);
            }
        });

        NavFragmentData();
        NavLoadData();

    }

    private void EditDataPassword(String clave, String clave2,String uid) {
        JSONObject jsonParams = new JSONObject();

        String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if ((!clave.isEmpty()) && (!clave2.isEmpty())){


            if (clave.equals(clave2)){

                ProgressDialog pDialog = new ProgressDialog(EditProfile.this);
                pDialog.setMessage("Cargando...");
                pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
                pDialog.show();

                try {
                    jsonParams.put("clave", clave);

                } catch ( JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                        Request.Method.PUT,
                        URL,
                        jsonParams,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                pDialog.dismiss();

                                alert(R.layout.my_success_dialog,"Contraseña cambiada correctamente.");

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                pDialog.dismiss();

                                NetworkResponse networkResponse = error.networkResponse;

                                if (networkResponse != null && networkResponse.data != null) {
                                    String jsonError = new String(networkResponse.data);
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject(jsonError);

                                        Log.e("e", String.valueOf(jsonObject));

                                        //alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }
                ){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("x-token", user.getString("token", ""));

                        return params;
                    }
                };
                requestQueue.add(jsonObjectRequest);

            }else {
                alert(R.layout.my_failed_dialog,"¡Las contraseñas no coincide!.");
            }

        }else{

            alert(R.layout.my_failed_dialog,"¡Los campos no pueden estar vacios!.");
        }
    }

    private void EditDataImgProfile(String img, String uid) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(EditProfile.this);
        AlertDialog alertDialog;

        LinearLayout vertical=new LinearLayout(EditProfile.this);
        vertical.setOrientation(LinearLayout.VERTICAL);
        vertical.setPadding(50,30,50,50);

        TextView txtEditTitle=new TextView(EditProfile.this);
        txtEditTitle.setText("Editar foto de perfil");
        txtEditTitle.setTextSize(27);
        txtEditTitle.setGravity(Gravity.CENTER);
        txtEditTitle.setTypeface(null, Typeface.BOLD);
        txtEditTitle.setPadding(20,20,20,20);
        vertical.addView(txtEditTitle);

        image.setBackgroundResource(R.drawable.defecto);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 400);
        image.setLayoutParams(layoutParams);
        if(image.getParent() != null) {
            ((ViewGroup)image.getParent()).removeView(image); // <- fix
        }
        vertical.addView(image);

        View viewDivider = new View(EditProfile.this);
        int dividerHeight = (int) (getResources().getDisplayMetrics().density * 20); // 1dp to pixels
        viewDivider.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider);

        btnEditImageCancel.setText("Cancelar");
        btnEditImageCancel.layout(50,20,50,20);
        btnEditImageCancel.setBackgroundResource(R.drawable.button_background_warning);
        if(btnEditImageCancel.getParent() != null) {
            ((ViewGroup)btnEditImageCancel.getParent()).removeView(btnEditImageCancel); // <- fix
        }
        vertical.addView(btnEditImageCancel);

        View viewDivider2 = new View(EditProfile.this);
        viewDivider2.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider2);

        Button btnEditImageAccept=new Button(EditProfile.this);
        btnEditImageAccept.setText("Subir");
        btnEditImageAccept.setPadding(50,20,50,20);
        btnEditImageAccept.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditImageAccept);

        dialog.setView(vertical);
        alertDialog=dialog.create();
        alertDialog.show();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });

        btnEditImageCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnEditImageAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                updateImage(uid);
            }
        });

    }

    private void updateImage(String uid) {

        JSONObject jsonParams = new JSONObject();

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/upload/usuario/"+uid;

        if (bitmap != null){

            ProgressDialog pDialog = new ProgressDialog(EditProfile.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("uri", mConvertImageToString(bitmap));

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            Log.e("json",jsonParams.toString());

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.PUT,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                           ManagementActivities.restartActivity(EditProfile.this);

                            try {
                                SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);
                                SharedPreferences.Editor userAccount = user.edit();
                                userAccount.putString("img",response.get("img").toString());
                                userAccount.commit();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    Log.e("d",jsonObject.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            /*finish();
                            startActivity(getIntent());*/

                        }
                    }
            );
            requestQueue.add(jsonObjectRequest);

        }else{
            alert(R.layout.my_failed_dialog,"¡Por favor suba una imagen.!");
        }

    }

    private String mConvertImageToString(Bitmap imageServer){
        try {
            Bitmap bitmap = null;
            if(this.isGallery){
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), this.imageServer);
                bitmap = getRezidBitmap(bitmap, 250);
            } else if(this.isCamera){
                File file = new File(this.currentPhotoPath);
                Uri uri = Uri.fromFile(file);
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                bitmap = getRezidBitmap(bitmap, 250);
            }
            ByteArrayOutputStream array=new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,75,array);
            byte[] imagenByte=array.toByteArray();
            String imagenString= Base64.encodeToString(imagenByte,Base64.DEFAULT);
            return "data:image/png;base64," + imagenString.replace(" ", "").replace("\n", "");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap getRezidBitmap (Bitmap bitmap, int maxSize){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        if(width <= maxSize && width <= maxSize){
            return bitmap;
        }

        float bitmapRatio = (float) width / (float) height;
        if(bitmapRatio > 1){
            width = maxSize;
            height = (int) (width/bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(bitmap,width, height, true);

    }

    private void loadImage() {
        final CharSequence[] opciones={"Tomar Foto","Elegir de Galeria","Cancelar"};
        final AlertDialog.Builder builder=new AlertDialog.Builder(EditProfile.this);
        builder.setTitle("Elige una opcion: ");
        builder.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("Tomar Foto")){
                    openCamera();
                }else if(opciones[i].equals("Elegir de Galeria")){
                    Intent intent=new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/");
                    startActivityIfNeeded(intent.createChooser(intent,"Seleccione"),COD_SELECCIONAR);
                }else if(opciones[i].equals("Cancelar")){
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(cameraIntent.resolveActivity(getPackageManager())!= null){

            File fotoFile = null;
            try {
                fotoFile = createFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(fotoFile != null){
                Uri photoUri = FileProvider.getUriForFile(
                        EditProfile.this,
                        getPackageName()+".provider",
                        fotoFile
                );
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
                startActivityForResult(cameraIntent,COD_FOTO);
            }
        }
    }

    private File createFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HH-mm-ss", Locale.getDefault()).format(new Date());
        String imgFileName = "IMG_" + timeStamp + "_";
        File storageFile = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imgFileName,
                ".png",
                storageFile
        );

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_CANCELED){

            switch (requestCode){
                case COD_SELECCIONAR:
                    Uri miPath=data.getData();
                    image.setImageURI(miPath);
                    this.isGallery = true;

                    imageServer=miPath;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
                case COD_FOTO:

                    image.setImageURI(Uri.parse(currentPhotoPath));
                    imageServer = Uri.parse(currentPhotoPath);
                    this.isCamera = true;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
            }

        }

    }

    private void EditDataProfile(String name, String lastName, String email, String phone, String id,String uid) {

        JSONObject jsonParams = new JSONObject();

        String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if ((!name.isEmpty()) && (!lastName.isEmpty()) && (!email.isEmpty())){

            ProgressDialog pDialog = new ProgressDialog(EditProfile.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("nombre", name);
                jsonParams.put("apellido", lastName);
                jsonParams.put("correo", email);
                jsonParams.put("telefono", phone);
                jsonParams.put("identificacion", id);

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.PUT,
                    URL,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                            ManagementActivities.restartActivity(EditProfile.this);

                            SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);
                            SharedPreferences.Editor userAccount = user.edit();
                            userAccount.putString("identificacion",id);
                            userAccount.putString("nombre",name);
                            userAccount.putString("apellido",lastName);
                            userAccount.putString("correo",email);
                            userAccount.putString("telefono",phone);
                            userAccount.commit();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    Log.e("e", String.valueOf(jsonObject));

                                        //alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            ){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Algunos campos no pueden estar vacios!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(EditProfile.this,myLayout,description);
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        recreate();
    }

    public void ClickHomeClient(View view){
        ManagementActivities.redirectActivity(this,SessionClient.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickHomeAuxiliary(View view){
        ManagementActivities.redirectActivity(this,SessionAuxiliary.class);
    }

    public void ClickReservation(View view){
        ManagementActivities.redirectActivity(this,Reservation.class);
    }

    public void ClickPayment(View view){
        ManagementActivities.redirectActivity(this,Payment.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }

    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(EditProfile.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();
        NavClientFragment=new NavClientFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
        }else if (rol.equals("USER")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavClientFragment).commit();
        }

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        image=(ImageView) findViewById(R.id.image);
        edtEditNameProfile=(EditText)findViewById(R.id.edtEditNameProfile);
        edtEditLastNameProfile=(EditText)findViewById(R.id.edtEditLastNameProfile);
        edtEditEmailProfile=(EditText)findViewById(R.id.edtEditEmailProfile);
        edtEditPhoneProfile=(EditText)findViewById(R.id.edtEditPhoneProfile);
        edtEditIdProfile=(EditText)findViewById(R.id.edtEditIdProfile);
        edtEditPassword=(EditText)findViewById(R.id.edtEditPassword);
        edtEditPassword2=(EditText)findViewById(R.id.edtEditPassword2);
        btnEditImgProfile=(Button) findViewById(R.id.btnEditImgProfile);
        btnEditProfile=(Button) findViewById(R.id.btnEditProfile);
        btnEditPassword=(Button) findViewById(R.id.btnEditPassword);
        EditImgProfile=(ImageView) findViewById(R.id.EditImgProfile);
        requestQueue= Volley.newRequestQueue(this);
        image=new ImageView(EditProfile.this);
        btnEditImageCancel=new AppCompatButton(EditProfile.this);
        btnEditImageAccept=new Button(EditProfile.this);

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(EditImgProfile);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(EditImgProfile);
        }

        edtEditNameProfile.setText(user.getString("nombre", ""));
        edtEditLastNameProfile.setText(user.getString("apellido", ""));
        edtEditEmailProfile.setText(user.getString("correo",""));
        edtEditPhoneProfile.setText(user.getString("telefono",""));
        edtEditIdProfile.setText(user.getString("identificacion",""));
        if (!user.getString("rol","").equals("AUX")){
            LinearLayout.LayoutParams layoutRow = new LinearLayout.LayoutParams(0, 0);
            edtEditIdProfile.setVisibility(View.GONE);
            edtEditIdProfile.setLayoutParams(layoutRow);
            edtEditIdProfile.setPadding(0,0,0,0);
            edtEditIdProfile.layout(0,0,0,0);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}