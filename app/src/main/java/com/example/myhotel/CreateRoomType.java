package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateRoomType extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName;
    ImageView img;
    TextInputEditText edtCreateCategoryRoomType,edtCreateBedRoomType,edtCreatePriceRoomType;
    CheckBox cbCreateTerraceRoomType;
    Button btnCreateRoomType;
    RequestQueue requestQueue;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/tipoH/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room_type);

        InitializeElements();
        btnCreateRoomType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String categoria=edtCreateCategoryRoomType.getText().toString();
                String camas= edtCreateBedRoomType.getText().toString();
                String precio=edtCreatePriceRoomType.getText().toString();
                boolean terraza=cbCreateTerraceRoomType.isChecked();

                Create(categoria,camas,precio,terraza);

            }
        });
        NavLoadData();
        NavFragmentData();

    }

    public void Create(String categoria, String camas, String precio, boolean terraza) {

        JSONObject jsonParams = new JSONObject();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(CreateRoomType.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        if ((!categoria.isEmpty()) && (!camas.isEmpty()) && (!precio.isEmpty())){

            try {
                jsonParams.put("categoria", categoria);
                jsonParams.put("camas", Integer.parseInt(camas));
                jsonParams.put("precio", Integer.parseInt(precio));
                jsonParams.put("terraza", terraza);

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.POST,
                    URL,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                            alert(R.layout.my_success_dialog,"¡Tipo de habitacion creada exitosamente!");

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                Log.e("error",jsonError);

                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            ){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(CreateRoomType.this,myLayout,description);
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickCreateRoomType(View view){
        recreate();
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(CreateRoomType.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        edtCreateCategoryRoomType=(TextInputEditText) findViewById(R.id.edtCreateCategoryRoomType);
        edtCreateBedRoomType=(TextInputEditText) findViewById(R.id.edtCreateBedRoomType);
        edtCreatePriceRoomType=(TextInputEditText) findViewById(R.id.edtCreatePriceRoomType);
        cbCreateTerraceRoomType=(CheckBox) findViewById(R.id.cbCreateTerraceRoomType);
        btnCreateRoomType=(Button) findViewById(R.id.btnCreateRoomType);
        requestQueue= Volley.newRequestQueue(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}