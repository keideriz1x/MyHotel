package com.example.myhotel;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassword extends AppCompatActivity {

    RequestQueue requestQueue;
    TextInputEditText edtForgotEmail;
    Button btnForgot;
    AlertDialog.Builder builderDialog;
    AlertDialog alertDialog;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/auth/olvido";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        InitializeElements();
        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email=edtForgotEmail.getText().toString().toLowerCase().trim();
                Sesion(Email);
            }
        });
    }

    public void Sesion(String correo) {

        JSONObject jsonParams = new JSONObject();

        if (!correo.isEmpty()){

            try {
                jsonParams.put("correo", correo);
            } catch ( JSONException e) {
                e.printStackTrace();
            }

            ProgressDialog pDialog = new ProgressDialog(ForgotPassword.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.PUT,
                    URL,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONObject jsonObject= null;

                            try {

                                pDialog.dismiss();

                                alert(R.layout.my_success_dialog,response.get("message").toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            );
            requestQueue.add(jsonObjectRequest);
        }else{
            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(ForgotPassword.this,myLayout,description);
    }

    public void InitializeElements(){
        requestQueue= Volley.newRequestQueue(this);
        edtForgotEmail= (TextInputEditText) findViewById(R.id.edtForgotEmail);
        btnForgot=(Button)findViewById(R.id.btnForgot);
    }

}