package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Register extends AppCompatActivity {

    TextInputEditText edtNameRegister,edtLastNameRegister,edtEmailRegister,edtPasswordRegister, edtPasswordRegister2;
    Button btnRegister;
    RequestQueue requestQueue;
    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        InitializeElements();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Name=edtNameRegister.getText().toString().toUpperCase();
                String LastName=edtLastNameRegister.getText().toString().toUpperCase();
                String Email=edtEmailRegister.getText().toString().toLowerCase().trim();
                String Password=edtPasswordRegister.getText().toString();
                String Password2=edtPasswordRegister2.getText().toString();

                Register(Name,LastName,Email,Password,Password2);
            }
        });
    }

    public void Register(String nombre, String apellido, String correo, String clave, String clave2) {

        JSONObject jsonParams = new JSONObject();

        if ((!nombre.isEmpty()) && (!apellido.isEmpty()) && (!correo.isEmpty()) && (!clave.isEmpty()) && (!clave2.isEmpty())){

            if (clave.equals(clave2)){

                try {
                    jsonParams.put("nombre", nombre);
                    jsonParams.put("apellido", apellido);
                    jsonParams.put("correo", correo);
                    jsonParams.put("clave", clave);
                    jsonParams.put("rol", "USER");

                } catch ( JSONException e) {
                    e.printStackTrace();
                }

                ProgressDialog pDialog = new ProgressDialog(Register.this);
                pDialog.setMessage("Cargando...");
                pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
                pDialog.show();

                JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                        Request.Method.POST,
                        URL,
                        jsonParams,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                pDialog.dismiss();

                                alert(R.layout.my_success_dialog,"¡Cuenta creada exitosamente!");

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                pDialog.dismiss();

                                NetworkResponse networkResponse = error.networkResponse;

                                if (networkResponse != null && networkResponse.data != null) {
                                    String jsonError = new String(networkResponse.data);
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject(jsonError);

                                        JSONObject jsonObject2=new JSONObject(jsonObject.get("errors").toString());
                                        JSONArray jsonArray=new JSONArray(jsonObject2.get("errors").toString());
                                        JSONObject jsonObject3=new JSONObject(jsonArray.get(0).toString());

                                        alert(R.layout.my_failed_dialog,jsonObject3.get("msg").toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }
                );
                requestQueue.add(jsonObjectRequest);

            }else{
                alert(R.layout.my_failed_dialog,"¡Las contraseñas no coinciden!.");
            }

        }else{
            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(Register.this,myLayout,description);
    }

    private void InitializeElements() {
        requestQueue= Volley.newRequestQueue(this);
        edtNameRegister=(TextInputEditText) findViewById(R.id.edtNameRegister);
        edtLastNameRegister=(TextInputEditText) findViewById(R.id.edtLastNameRegister);
        edtEmailRegister=(TextInputEditText) findViewById(R.id.edtEmailRegister);
        edtPasswordRegister=(TextInputEditText) findViewById(R.id.edtPasswordRegister);
        edtPasswordRegister2=(TextInputEditText) findViewById(R.id.edtPasswordRegister2);
        btnRegister=(Button) findViewById(R.id.btnRegister);
    }

}