package com.example.myhotel;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MoreInfoAuxiliary extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName,txtNameAuxiliary,txtEmailAuxiliary,txtStateAuxiliary,txtPhoneAuxiliary;
    RequestQueue requestQueue;
    ImageView img,imgAuxiliary;
    Button btnStateChangeAuxiliary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info_auxiliary);

        InitializeElements();

        String uid = getIntent().getExtras().getString("uid");
        String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario/"+uid;

        ConsultDataAuxiliary(URL,uid);

        NavLoadData();
        NavFragmentData();

    }

    private void ConsultDataAuxiliary(String URL, String uid) {

        ProgressDialog pDialog = new ProgressDialog(MoreInfoAuxiliary.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {
                            JSONObject jsonObject= new JSONObject(response.get("resp").toString());

                            if (!jsonObject.get("img").equals("")) {
                                Picasso.get()
                                        .load(jsonObject.get("img").toString())
                                        .into(imgAuxiliary);
                            }else{
                                Picasso.get()
                                        .load(R.drawable.defecto)
                                        .into(imgAuxiliary);
                            }

                            txtNameAuxiliary.setText("Auxiliar "+jsonObject.get("nombre").toString()+" "+jsonObject.get("apellido").toString());
                            txtEmailAuxiliary.setText("Correo: "+jsonObject.get("correo").toString());

                            if (jsonObject.get("estado").toString().equals("true")){
                                txtStateAuxiliary.setText("Estado: Habilitado.");
                            }else{
                                txtStateAuxiliary.setText("Estado: Deshabilitado.");
                            }

                            if (!jsonObject.get("telefono").toString().isEmpty()){
                                txtPhoneAuxiliary.setText(jsonObject.get("telefono").toString());
                            }

                            btnStateChangeAuxiliary.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    warningAlert(R.layout.my_warning_dialog,"¿Esta seguro de cambiar el estado?",uid);
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void warningAlert(int myLayout, String description, String uid) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(MoreInfoAuxiliary.this);
        View layoutView=getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                StateChange(uid);

            }
        });

    }

    public void StateChange(String uid){
        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/usuario/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(MoreInfoAuxiliary.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.DELETE,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        ManagementActivities.restartActivity(MoreInfoAuxiliary.this);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(MoreInfoAuxiliary.this,myLayout,descriptipn);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(MoreInfoAuxiliary.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();

    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        txtNameAuxiliary=(TextView)findViewById(R.id.txtNameAuxiliary);
        txtEmailAuxiliary=(TextView)findViewById(R.id.txtEmailAuxiliary);
        txtStateAuxiliary=(TextView)findViewById(R.id.txtStateAuxiliary);
        txtPhoneAuxiliary=(TextView)findViewById(R.id.txtPhoneAuxiliary);
        drawerLayout=findViewById(R.id.drawer_layout);
        requestQueue= Volley.newRequestQueue(this);
        img=(ImageView) findViewById(R.id.img);
        imgAuxiliary=(ImageView) findViewById(R.id.imgAuxiliary);
        btnStateChangeAuxiliary=(Button) findViewById(R.id.btnStateChangeAuxiliary);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}