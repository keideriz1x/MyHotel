package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CreateService extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName;
    ImageView img;
    EditText edtNameService,edtDescriptionService,edtPriceService;
    Button btnCreateService;
    RequestQueue requestQueue;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/servicio/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_service);

        InitializeElements();
        btnCreateService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name=edtNameService.getText().toString();
                String Description=edtDescriptionService.getText().toString();
                String  Price=edtPriceService.getText().toString();

                Create(Name,Description,Price);

            }
        });
        NavLoadData();
        NavFragmentData();

    }

    public void Create(String Name, String Description, String Price) {

        JSONObject jsonParams = new JSONObject();

        ProgressDialog pDialog = new ProgressDialog(CreateService.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        if ((!Name.isEmpty()) && (!Description.isEmpty()) && (!Price.isEmpty())){

            try {
                jsonParams.put("nombre", Name);
                jsonParams.put("descripcion", Description);
                jsonParams.put("precio", Integer.parseInt(Price));

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.POST,
                    URL,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                            alert(R.layout.my_success_dialog,"¡Cuenta creada exitosamente!");

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    Log.e("e", String.valueOf(jsonObject));

                                        //alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            );
            requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(CreateService.this,myLayout,description);
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickCreateService(View view){
        recreate();
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(CreateService.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        edtNameService=(EditText) findViewById(R.id.edtNameService);
        edtDescriptionService=(EditText) findViewById(R.id.edtDescriptionService);
        edtPriceService=(EditText) findViewById(R.id.edtPriceService);
        btnCreateService=(Button) findViewById(R.id.btnCreateService);
        requestQueue= Volley.newRequestQueue(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}