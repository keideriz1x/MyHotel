package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Payment extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavClientFragment;
    TextView txtName,txtConsultPayment,tvFecha,tvPrecio,txtSeeMore;
    ImageView img;
    TableRow fila;
    Button btnSeeMore;
    TableLayout tableLayout;
    ArrayList<FacturaArrayList> arrayList=new ArrayList<>();
    RequestQueue requestQueue;
    TemplatePDF templatePDF=new TemplatePDF(Payment.this);
    ArrayList<BillArrayList> arrayList2=new ArrayList<>();

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/factura/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        InitializeElements();

        ConsultDataReservation();

        NavLoadData();
        NavFragmentData();

    }

    public void ConsultDataReservation(){

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(Payment.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            pDialog.dismiss();

                            JSONArray facturacion=new JSONArray(response.get("facturacion").toString());

                            final int total = facturacion.length();

                            if (total>=1){

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(facturacion.get(i).toString());

                                    String[] fecha=jsonObject.get("fecha").toString().split("T");
                                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                                    String price = formatter.format(jsonObject.get("valor_total"));

                                    arrayList.add(new FacturaArrayList(fecha[0],"COP "+price,jsonObject.get("uid").toString()));

                                }

                                generateTable(arrayList);

                            }else{

                                txtConsultPayment.setTextSize(18);
                                txtConsultPayment.setGravity(Gravity.CENTER);
                                txtConsultPayment.setText("No se encontro información de reservas.");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private void generateTable(ArrayList<FacturaArrayList> arrayList) {

        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,45);
        TableRow.LayoutParams layoutEmail = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,45);
        TableRow.LayoutParams layoutSeeMore = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,10);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(Payment.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvFecha = new TextView(Payment.this);
                tvFecha.setText("FECHA");
                tvFecha.setTextSize(18);
                tvFecha.setGravity(Gravity.CENTER);
                tvFecha.setBackgroundColor(Color.parseColor("#1976D2"));
                tvFecha.setTextColor(Color.WHITE);
                tvFecha.setPadding(10, 10, 10, 10);
                tvFecha.setLayoutParams(layoutName);
                fila.addView(tvFecha);

                tvPrecio = new TextView(Payment.this);
                tvPrecio.setText("PRECIO");
                tvPrecio.setTextSize(18);
                tvPrecio.setGravity(Gravity.CENTER);
                tvPrecio.setBackgroundColor(Color.parseColor("#1976D2"));
                tvPrecio.setTextColor(Color.WHITE);
                tvPrecio.setPadding(10, 10, 10, 10);
                tvPrecio.setLayoutParams(layoutName);
                fila.addView(tvPrecio);

                txtSeeMore = new TextView(Payment.this);
                txtSeeMore.setTextSize(18);
                txtSeeMore.setGravity(Gravity.CENTER);
                txtSeeMore.setBackgroundColor(Color.parseColor("#1976D2"));
                txtSeeMore.setTextColor(Color.WHITE);
                txtSeeMore.setPadding(10, 10, 10, 10);
                txtSeeMore.setLayoutParams(layoutName);
                fila.addView(txtSeeMore);

                tableLayout.addView(fila);
            } else {
                tvFecha = new TextView(Payment.this);
                tvFecha.setTextSize(16);
                tvFecha.setGravity(Gravity.CENTER);
                tvFecha.setText(arrayList.get(i).getFecha());
                tvFecha.setPadding(10, 10, 10, 10);
                tvFecha.setHorizontallyScrolling(true);
                tvFecha.setEllipsize(TextUtils.TruncateAt.END);
                tvFecha.setMaxLines(1);
                tvFecha.setLayoutParams(layoutName);
                fila.addView(tvFecha);

                tvPrecio = new TextView(Payment.this);
                tvPrecio.setTextSize(16);
                tvPrecio.setGravity(Gravity.CENTER);
                tvPrecio.setText(arrayList.get(i).getPrecio());
                tvPrecio.setPadding(10, 10, 10, 10);
                tvPrecio.setHorizontallyScrolling(true);
                tvPrecio.setEllipsize(TextUtils.TruncateAt.END);
                tvPrecio.setMaxLines(1);
                tvPrecio.setLayoutParams(layoutEmail);
                fila.addView(tvPrecio);

                btnSeeMore = new Button(Payment.this);
                btnSeeMore.setGravity(Gravity.CENTER);
                btnSeeMore.setBackgroundColor(Color.TRANSPARENT);
                btnSeeMore.setText("Ver mas...");
                btnSeeMore.setTextColor(Color.parseColor("#1976D2"));
                int finalI = i;
                btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        generateBill(arrayList.get(finalI).getUid());
                    }
                });
                btnSeeMore.setPadding(10, 10, 10, 10);
                btnSeeMore.setLayoutParams(layoutSeeMore);
                fila.addView(btnSeeMore);

                tableLayout.addView(fila);

            }
        }

    }

    private void generateBill(String uid) {

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/factura/"+uid;

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        Log.e("URL2",URL2);
        Log.e("token",user.getString("token", ""));

        ProgressDialog pDialog = new ProgressDialog(Payment.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {

                            JSONObject data=new JSONObject(response.get("data").toString());
                            JSONObject usuario=new JSONObject(data.get("usuario").toString());
                            JSONObject facturacion=new JSONObject(data.get("facturacion").toString());
                            JSONObject reserva=new JSONObject(data.get("reserva").toString());
                            JSONObject tipo_habitacion=new JSONObject(data.get("tipo_habitacion").toString());
                            JSONArray gastosExtra=new JSONArray(data.get("gastosExtra").toString());
                            final int total = gastosExtra.length();

                            String[] headerUsuario={"Nombre","Apellido","Correo"};
                            String[] headerReserva={"Tipo de habitación","Fecha de entrada","Fecha de salida","Total de reserva"};
                            String[] headerServicio={"Nombre","Fecha de servicio","Precio"};

                            templatePDF.openDocument();
                            templatePDF.addMetadaData("My Hotel","Factura","Julian Rincon");
                            String[] fecha=facturacion.get("fecha").toString().split("T");
                            templatePDF.addTitles("My Hotel","No. factura :"+facturacion.get("id_facturacion").toString(),"No. reserva :"+reserva.get("uid").toString(),fecha[0]);

                            templatePDF.addSubTitles("Cliente");
                            templatePDF.createTable(headerUsuario,getClient(usuario.get("nombre").toString(),usuario.get("apellido").toString(),usuario.get("correo").toString()));

                            templatePDF.addSubTitles("Reserva");
                            String[] fechaInicio=reserva.get("fechaInicio").toString().split("T");
                            String[] fechaFin=reserva.get("fechaFin").toString().split("T");
                            DecimalFormat formatter = new DecimalFormat("#,###,###");
                            String price = formatter.format(reserva.get("precio"));
                            templatePDF.createTable(headerReserva,getReservation(tipo_habitacion.get("categoria").toString(),fechaInicio[0],fechaFin[0],"COP "+price));

                            templatePDF.addSubTitles("Servicios");

                            for (int i=0;i<total;i++){
                                JSONObject jsonService=new JSONObject(gastosExtra.get(i).toString());

                                arrayList2.add(new BillArrayList(jsonService.get("nombre").toString(),jsonService.get("fecha").toString(),"COP "+jsonService.get("precio").toString()));
                            }

                            templatePDF.createTable(headerServicio,getService(arrayList2));
                            String precio2 = formatter.format(reserva.get("precio"));

                            templatePDF.addTotal("Reserva: COP "+precio2,"Gastos extras: COP "+facturacion.get("gasto_extra_total").toString(),"Total: COP "+facturacion.get("valor_total").toString());

                            templatePDF.closeDocument();
                            templatePDF.viewPDF(Payment.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                Log.e("error",jsonObject.toString());

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private ArrayList<String[]> getService(ArrayList<BillArrayList> arrayList) {
        ArrayList<String[]>rows=new ArrayList<>();

        for (int i=0;i< arrayList.size();i++){
            rows.add(new String[]{arrayList.get(i).getNombre(),arrayList.get(i).getFecha(),arrayList.get(i).getPrecio()});
        }

        return  rows;
    }

    private ArrayList<String[]> getReservation(String categoria, String fechaInicio, String fechaFin, String precio) {
        ArrayList<String[]>rows=new ArrayList<>();

        for (int i=0;i< 1;i++){
            rows.add(new String[]{categoria,fechaInicio,fechaFin,precio});
        }

        return  rows;
    }

    private ArrayList<String[]> getClient(String nombre, String apellido, String correo) {
        ArrayList<String[]>rows=new ArrayList<>();

        for (int i=0;i< 1;i++){
            rows.add(new String[]{nombre,apellido,correo});
        }

        return  rows;
    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(Payment.this,myLayout,descriptipn);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeClient(View view){
        ManagementActivities.redirectActivity(this,SessionClient.class);
    }

    public void ClickReservation(View view){
        ManagementActivities.redirectActivity(this,Reservation.class);
    }

    public void ClickPayment(View view){
        recreate();
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(Payment.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavClientFragment=new NavClientFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavClientFragment).commit();
    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        tableLayout=(TableLayout)findViewById(R.id.tableLayout);
        txtConsultPayment=(TextView) findViewById(R.id.txtConsultPayment);
        requestQueue= Volley.newRequestQueue(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}