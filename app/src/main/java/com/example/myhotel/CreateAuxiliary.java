package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CreateAuxiliary extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextInputEditText edtNameAuxiliary,edtLastNameAuxiliary,edtCardAuxiliary,edtEmailAuxiliary;
    TextView txtName;
    ImageView img;
    RequestQueue requestQueue;
    Button btnCreateAuxiliary;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_auxiliary);

        InitializeElements();
        btnCreateAuxiliary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name=edtNameAuxiliary.getText().toString().toUpperCase();
                String LastName=edtLastNameAuxiliary.getText().toString().toUpperCase();
                String Email=edtEmailAuxiliary.getText().toString().toLowerCase().trim();
                String Card=edtCardAuxiliary.getText().toString();

                Create(Name,LastName,Email,Card);
            }
        });
        NavFragmentData();
        NavLoadData();

    }

    public void Create(String nombre, String apellido, String correo, String cedula) {

        JSONObject jsonParams = new JSONObject();

        if ((!nombre.isEmpty()) && (!apellido.isEmpty()) && (!correo.isEmpty()) && (!cedula.isEmpty())){

            ProgressDialog pDialog = new ProgressDialog(CreateAuxiliary.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

                try {
                    jsonParams.put("nombre", nombre);
                    jsonParams.put("apellido", apellido);
                    jsonParams.put("correo", correo);
                    jsonParams.put("clave", cedula);
                    jsonParams.put("identificacion", cedula);
                    jsonParams.put("rol", "AUX");

                } catch ( JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                        Request.Method.POST,
                        URL,
                        jsonParams,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                pDialog.dismiss();

                                alert(R.layout.my_success_dialog,"¡Cuenta creada exitosamente!");

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                pDialog.dismiss();

                                NetworkResponse networkResponse = error.networkResponse;

                                if (networkResponse != null && networkResponse.data != null) {
                                    String jsonError = new String(networkResponse.data);
                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject(jsonError);

                                        Log.e("e", String.valueOf(jsonObject));

                                        if (jsonObject.has("errors")){
                                            JSONObject jsonObject2=new JSONObject(jsonObject.get("errors").toString());
                                            JSONArray jsonArray=new JSONArray(jsonObject2.get("errors").toString());
                                            JSONObject jsonObject3=new JSONObject(jsonArray.get(0).toString());

                                            alert(R.layout.my_failed_dialog,jsonObject3.get("msg").toString());
                                        }else {

                                            alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }
                );
                requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(CreateAuxiliary.this,myLayout,description);
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickCreateAuxiliary(View view){
        recreate();
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(CreateAuxiliary.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        requestQueue= Volley.newRequestQueue(this);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        edtNameAuxiliary=(TextInputEditText) findViewById(R.id.edtNameAuxiliary);
        edtLastNameAuxiliary=(TextInputEditText) findViewById(R.id.edtLastNameAuxiliary);
        edtCardAuxiliary=(TextInputEditText) findViewById(R.id.edtCardAuxiliary);
        edtEmailAuxiliary=(TextInputEditText) findViewById(R.id.edtEmailAuxiliary);
        btnCreateAuxiliary=(Button) findViewById(R.id.btnCreateAuxiliary);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}