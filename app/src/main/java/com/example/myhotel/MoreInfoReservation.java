package com.example.myhotel;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MoreInfoReservation extends AppCompatActivity {

    DrawerLayout drawerLayout;
    TextView txtName,txtRoomReservation,txtDateInitReservation,txtDateFinReservation,txtPriceReservation,txtRoomTypeReservation,txtStateReservation;
    RequestQueue requestQueue;
    ImageView img,imgReservation;
    Fragment NavClientFragment;
    Button btnCancelReservation,btnAddService,btnFinReservation;
    TemplatePDF templatePDF=new TemplatePDF(MoreInfoReservation.this);
    ArrayList<BillArrayList> arrayList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info_reservation);

        InitializeElements();

        String uid = getIntent().getExtras().getString("uid");
        String URL="https://udmyhotelproject.herokuapp.com/myhotel/reserva/"+uid;

        ConsultReservation(URL,uid);

        pedirPermisos();

        NavLoadData();
        NavFragmentData();

    }

    public void pedirPermisos(){
        if(ContextCompat.checkSelfPermission(MoreInfoReservation.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(MoreInfoReservation.this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    public void ConsultReservation(String URL, String uid){

        ProgressDialog pDialog = new ProgressDialog(MoreInfoReservation.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {
                            JSONObject jsonObject= new JSONObject(response.get("tipo_habitacion").toString());
                            JSONObject jsonObject2= new JSONObject(response.get("reserva").toString());
                            JSONObject jsonObject3= new JSONObject(jsonObject2.get("habitacion").toString());

                            if (!jsonObject.get("img").equals("")) {
                                Picasso.get()
                                        .load(jsonObject.get("img").toString())
                                        .into(imgReservation);
                            }else{
                                Picasso.get()
                                        .load(R.drawable.defecto)
                                        .into(imgReservation);
                            }

                            String[] fechaInicio=jsonObject2.get("fechaInicio").toString().split("T");
                            String[] fechaFin=jsonObject2.get("fechaFin").toString().split("T");

                            txtRoomReservation.setText("Habitacion "+jsonObject3.get("numero").toString());
                            txtDateInitReservation.setText("Fecha de Inicio: "+fechaInicio[0]);
                            txtDateFinReservation.setText("Fecha de finalización: "+fechaFin[0]);

                            DecimalFormat formatter = new DecimalFormat("#,###,###");
                            String price = formatter.format(jsonObject2.get("precio"));

                            txtPriceReservation.setText("Precio de reserva: COP "+price);
                            txtRoomTypeReservation.setText("Tipo de Habitación: "+jsonObject.get("categoria").toString());

                            if (jsonObject3.get("ocupado").toString().equals("true")){
                                txtStateReservation.setText("Estado: En reserva");
                            }else{
                                txtStateReservation.setText("Estado: Sin reserva");
                            }

                            btnCancelReservation.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    warningAlert(R.layout.my_warning_dialog,"¿Esta seguro de cancelar la reserva?",uid);
                                }
                            });

                            btnAddService.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        Intent intent = new Intent(MoreInfoReservation.this, AddService.class);
                                        intent.putExtra("uid", jsonObject2.get("uid").toString());
                                        startActivity(intent);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            btnFinReservation.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        generateBill(jsonObject2.get("uid").toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void generateBill(String uid) {

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/factura/"+uid;

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(MoreInfoReservation.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.POST,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {

                            ManagementActivities.restartActivity(MoreInfoReservation.this);

                            JSONObject data=new JSONObject(response.get("data").toString());
                            JSONObject usuario=new JSONObject(data.get("usuario").toString());
                            JSONObject facturacion=new JSONObject(data.get("facturacion").toString());
                            JSONObject reserva=new JSONObject(data.get("reserva").toString());
                            JSONObject tipo_habitacion=new JSONObject(data.get("tipo_habitacion").toString());
                            JSONArray gastosExtra=new JSONArray(data.get("gastosExtra").toString());
                            final int total = gastosExtra.length();

                            String[] headerUsuario={"Nombre","Apellido","Correo"};
                            String[] headerReserva={"Tipo de habitación","Fecha de entrada","Fecha de salida","Total de reserva"};
                            String[] headerServicio={"Nombre","Fecha de servicio","Precio"};

                            templatePDF.openDocument();
                            templatePDF.addMetadaData("My Hotel","Factura","Julian Rincon");
                            templatePDF.addTitles("My Hotel","No. factura :"+facturacion.get("id_facturacion").toString(),"No. reserva :"+reserva.get("uid").toString(),facturacion.get("fecha").toString());

                            templatePDF.addSubTitles("Cliente");
                            templatePDF.createTable(headerUsuario,getClient(usuario.get("nombre").toString(),usuario.get("apellido").toString(),usuario.get("correo").toString()));

                            templatePDF.addSubTitles("Reserva");
                            String[] fechaInicio=reserva.get("fechaInicio").toString().split("T");
                            String[] fechaFin=reserva.get("fechaFin").toString().split("T");
                            DecimalFormat formatter = new DecimalFormat("#,###,###");
                            String price = formatter.format(reserva.get("precio"));
                            templatePDF.createTable(headerReserva,getReservation(tipo_habitacion.get("categoria").toString(),fechaInicio[0],fechaFin[0],"COP "+price));

                            templatePDF.addSubTitles("Servicios");

                            for (int i=0;i<total;i++){
                                JSONObject jsonService=new JSONObject(gastosExtra.get(i).toString());

                                String precio = formatter.format(jsonService.get("precio"));
                                arrayList.add(new BillArrayList(jsonService.get("nombre").toString(),jsonService.get("fecha").toString(),"COP "+precio));
                            }
                            
                            templatePDF.createTable(headerServicio,getService(arrayList));
                            String precio2 = formatter.format(reserva.get("precio"));
                            String precio3 = formatter.format(facturacion.get("valor_total"));

                            templatePDF.addTotal("Reserva: COP "+precio2,"Gastos extras: COP "+facturacion.get("gasto_extra_total").toString(),"Total: COP "+precio3);

                            templatePDF.closeDocument();
                            templatePDF.viewPDF(MoreInfoReservation.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private ArrayList<String[]> getService(ArrayList<BillArrayList> arrayList) {
        ArrayList<String[]>rows=new ArrayList<>();

        for (int i=0;i< arrayList.size();i++){
            rows.add(new String[]{arrayList.get(i).getNombre(),arrayList.get(i).getFecha(),arrayList.get(i).getPrecio()});
        }

        return  rows;
    }

    private ArrayList<String[]> getReservation(String categoria, String fechaInicio, String fechaFin, String precio) {
        ArrayList<String[]>rows=new ArrayList<>();

        for (int i=0;i< 1;i++){
            rows.add(new String[]{categoria,fechaInicio,fechaFin,precio});
        }

        return  rows;
    }

    private ArrayList<String[]> getClient(String nombre, String apellido, String correo) {
        ArrayList<String[]>rows=new ArrayList<>();

        for (int i=0;i< 1;i++){
            rows.add(new String[]{nombre,apellido,correo});
        }

        return  rows;
    }

    private void warningAlert(int myLayout, String description, String uid) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(MoreInfoReservation.this);
        View layoutView=getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                CancelReservation(uid);

            }
        });

    }

    private void CancelReservation(String uid) {
        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/reserva/cancelar/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(MoreInfoReservation.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.PUT,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        ManagementActivities.finishActivity(MoreInfoReservation.this,Reservation.class);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(MoreInfoReservation.this,myLayout,descriptipn);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeClient(View view){
        ManagementActivities.redirectActivity(this,SessionClient.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickPayment(View view){
        ManagementActivities.redirectActivity(this,Payment.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(MoreInfoReservation.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavClientFragment=new NavClientFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavClientFragment).commit();
    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        requestQueue= Volley.newRequestQueue(this);
        drawerLayout=findViewById(R.id.drawer_layout);
        txtName=(TextView) findViewById(R.id.txtName);
        txtRoomReservation=(TextView) findViewById(R.id.txtRoomReservation);
        txtDateInitReservation=(TextView) findViewById(R.id.txtDateInitReservation);
        txtDateFinReservation=(TextView) findViewById(R.id.txtDateFinReservation);
        txtPriceReservation=(TextView) findViewById(R.id.txtPriceReservation);
        txtRoomTypeReservation=(TextView) findViewById(R.id.txtRoomTypeReservation);
        txtStateReservation=(TextView) findViewById(R.id.txtStateReservation);
        img=(ImageView) findViewById(R.id.img);
        imgReservation=(ImageView) findViewById(R.id.imgReservation);
        btnCancelReservation=(Button) findViewById(R.id.btnCancelReservation);
        btnAddService=(Button) findViewById(R.id.btnAddService);
        btnFinReservation=(Button) findViewById(R.id.btnFinReservation);
    }

}