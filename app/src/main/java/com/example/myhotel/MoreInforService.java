package com.example.myhotel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MoreInforService extends AppCompatActivity {

    DrawerLayout drawerLayout;
    RequestQueue requestQueue;
    Button btnStateChangeService,btnEditService,btnEditImageService;
    ImageView imgService;
    TextView txtNameService,txtDescriptionService,txtPriceService,txtStateService,txtName;
    Fragment NavAdministratorFragment,NavAuxiliaryFragment;
    ImageView image,img;
    AppCompatButton btnEditImageCancel;
    Button btnEditImageAccept;
    private static final int COD_SELECCIONAR = 10;
    private static final int COD_FOTO = 20;
    String currentPhotoPath;
    Uri imageServer;
    boolean isCamera = false;
    boolean isGallery = false;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_infor_service);

        InitializeElements();

        String uid = getIntent().getExtras().getString("uid");

        String URL="https://udmyhotelproject.herokuapp.com/myhotel/servicio/"+uid;

        NavLoadData();
        NavFragmentData();

        ConsultService(URL,uid);

    }

    public void ConsultService(String URL,String uid){

        ProgressDialog pDialog = new ProgressDialog(MoreInforService.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {
                            JSONObject jsonObject= new JSONObject(response.get("servicio").toString());

                            if (!jsonObject.get("img").equals("")) {
                                Picasso.get()
                                        .load(jsonObject.get("img").toString())
                                        .into(imgService);
                            }else{
                                Picasso.get()
                                        .load(R.drawable.defecto)
                                        .into(imgService);
                            }

                            txtNameService.setText(jsonObject.get("nombre").toString());
                            txtDescriptionService.setText("Descripcion: "+jsonObject.get("descripcion").toString());
                            txtPriceService.setText("Precio: "+jsonObject.get("precio").toString());

                            if (jsonObject.get("estado").toString().equals("true")){
                                txtStateService.setText("Estado: Disponible");
                            }else {
                                txtStateService.setText("Estado: No disponible");
                            }

                            btnStateChangeService.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    warningAlert(R.layout.my_warning_dialog,"¿Esta seguro de cambiar el estado?",uid);
                                }
                            });

                            btnEditService.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        EditService(jsonObject.get("nombre").toString(),jsonObject.get("descripcion").toString(),jsonObject.get("precio").toString(),uid);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            btnEditImageService.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        EditImageService(jsonObject.get("img").toString(),uid);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);
                                Log.e("f",jsonObject.toString());


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void EditImageService(String img, String uid) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MoreInforService.this);
        AlertDialog alertDialog;

        LinearLayout vertical=new LinearLayout(MoreInforService.this);
        vertical.setOrientation(LinearLayout.VERTICAL);
        vertical.setPadding(50,30,50,50);

        TextView txtEditTitle=new TextView(MoreInforService.this);
        txtEditTitle.setText("Editar foto de servicio");
        txtEditTitle.setTextSize(27);
        txtEditTitle.setGravity(Gravity.CENTER);
        txtEditTitle.setTypeface(null, Typeface.BOLD);
        txtEditTitle.setPadding(20,20,20,20);
        vertical.addView(txtEditTitle);

        image.setBackgroundResource(R.drawable.defecto);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 400);
        image.setLayoutParams(layoutParams);
        if(image.getParent() != null) {
            ((ViewGroup)image.getParent()).removeView(image);
        }
        vertical.addView(image);

        View viewDivider = new View(MoreInforService.this);
        int dividerHeight = (int) (getResources().getDisplayMetrics().density * 20); // 1dp to pixels
        viewDivider.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider);

        btnEditImageCancel.setText("Cancelar");
        btnEditImageCancel.layout(50,20,50,20);
        btnEditImageCancel.setBackgroundResource(R.drawable.button_background_warning);
        if(btnEditImageCancel.getParent() != null) {
            ((ViewGroup)btnEditImageCancel.getParent()).removeView(btnEditImageCancel); // <- fix
        }
        vertical.addView(btnEditImageCancel);

        View viewDivider2 = new View(MoreInforService.this);
        viewDivider2.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider2);

        Button btnEditImageAccept=new Button(MoreInforService.this);
        btnEditImageAccept.setText("Subir");
        btnEditImageAccept.setPadding(50,20,50,20);
        btnEditImageAccept.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditImageAccept);

        dialog.setView(vertical);
        alertDialog=dialog.create();
        alertDialog.show();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });

        btnEditImageCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnEditImageAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                updateImage(uid);
            }
        });
    }

    private void updateImage(String uid) {

        JSONObject jsonParams = new JSONObject();

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/upload/servicio/"+uid;

        if (bitmap != null){
            ProgressDialog pDialog = new ProgressDialog(MoreInforService.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("uri", mConvertImageToString(bitmap));

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            Log.e("json",jsonParams.toString());

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.PUT,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                            ManagementActivities.restartActivity(MoreInforService.this);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    Log.e("e", String.valueOf(jsonObject));

                                    //alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            finish();
                            startActivity(getIntent());

                        }
                    }
            );
            requestQueue.add(jsonObjectRequest);
        }else {
            alert(R.layout.my_failed_dialog,"¡Por favor suba una imagen.!");
        }

    }

    private String mConvertImageToString(Bitmap imageServer){
        try {
            Bitmap bitmap = null;
            if(this.isGallery){
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), this.imageServer);
                bitmap = getRezidBitmap(bitmap, 250);
            } else if(this.isCamera){
                File file = new File(this.currentPhotoPath);
                Uri uri = Uri.fromFile(file);
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                bitmap = getRezidBitmap(bitmap, 250);
            }
            ByteArrayOutputStream array=new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,75,array);
            byte[] imagenByte=array.toByteArray();
            String imagenString= Base64.encodeToString(imagenByte,Base64.DEFAULT);
            return "data:image/png;base64," + imagenString.replace(" ", "").replace("\n", "");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap getRezidBitmap (Bitmap bitmap, int maxSize){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        if(width <= maxSize && width <= maxSize){
            return bitmap;
        }

        float bitmapRatio = (float) width / (float) height;
        if(bitmapRatio > 1){
            width = maxSize;
            height = (int) (width/bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(bitmap,width, height, true);

    }

    private void loadImage() {
        final CharSequence[] opciones={"Tomar Foto","Elegir de Galeria","Cancelar"};
        final AlertDialog.Builder builder=new AlertDialog.Builder(MoreInforService.this);
        builder.setTitle("Elige una opcion: ");
        builder.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("Tomar Foto")){
                    openCamera();
                }else if(opciones[i].equals("Elegir de Galeria")){
                    Intent intent=new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/");
                    startActivityIfNeeded(intent.createChooser(intent,"Seleccione"),COD_SELECCIONAR);
                }else if(opciones[i].equals("Cancelar")){
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(cameraIntent.resolveActivity(getPackageManager())!= null){

            File fotoFile = null;
            try {
                fotoFile = createFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(fotoFile != null){
                Uri photoUri = FileProvider.getUriForFile(
                        MoreInforService.this,
                        getPackageName()+".provider",
                        fotoFile
                );
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
                startActivityForResult(cameraIntent,COD_FOTO);
            }
        }
    }

    private File createFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HH-mm-ss", Locale.getDefault()).format(new Date());
        String imgFileName = "IMG_" + timeStamp + "_";
        File storageFile = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imgFileName,
                ".png",
                storageFile
        );

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_CANCELED){

            switch (requestCode){
                case COD_SELECCIONAR:
                    Uri miPath=data.getData();
                    image.setImageURI(miPath);
                    this.isGallery = true;

                    imageServer=miPath;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
                case COD_FOTO:

                    image.setImageURI(Uri.parse(currentPhotoPath));
                    imageServer = Uri.parse(currentPhotoPath);
                    this.isCamera = true;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
            }

        }

    }

    private void EditService(String nombre, String descripcion, String precio, String uid) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MoreInforService.this);
        AlertDialog alertDialog;

        LinearLayout vertical=new LinearLayout(MoreInforService.this);
        vertical.setOrientation(LinearLayout.VERTICAL);
        vertical.setPadding(50,30,50,50);

        TextView txtEditTitle=new TextView(MoreInforService.this);
        txtEditTitle.setText("Editar Servicio");
        txtEditTitle.setTextSize(27);
        txtEditTitle.setGravity(Gravity.CENTER);
        txtEditTitle.setTypeface(null, Typeface.BOLD);
        txtEditTitle.setPadding(20,20,20,20);
        vertical.addView(txtEditTitle);

        TextInputLayout nameTextInputLayout = new TextInputLayout(MoreInforService.this);
        nameTextInputLayout.setHint("Nombre");
        TextInputEditText edtEditName = new TextInputEditText(nameTextInputLayout.getContext());
        edtEditName.setText(nombre);
        nameTextInputLayout.addView(edtEditName);
        nameTextInputLayout.setPadding(0,20,0,20);
        vertical.addView(nameTextInputLayout);

        TextInputLayout descriptionTextInputLayout = new TextInputLayout(MoreInforService.this);
        descriptionTextInputLayout.setHint("Descripcion");
        TextInputEditText edtEditDescription = new TextInputEditText(descriptionTextInputLayout.getContext());
        edtEditDescription.setText(descripcion);
        descriptionTextInputLayout.addView(edtEditDescription);
        descriptionTextInputLayout.setPadding(0,20,0,20);
        vertical.addView(descriptionTextInputLayout);

        TextInputLayout priceTextInputLayout = new TextInputLayout(MoreInforService.this);
        priceTextInputLayout.setHint("Precio");
        TextInputEditText edtEditPrice = new TextInputEditText(priceTextInputLayout.getContext());
        edtEditPrice.setText(precio);
        edtEditPrice.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtEditPrice.setFilters(new InputFilter[] {new InputFilter.LengthFilter(6)});
        priceTextInputLayout.addView(edtEditPrice);
        priceTextInputLayout.setPadding(0,20,0,20);
        vertical.addView(priceTextInputLayout);

        View viewDivider = new View(MoreInforService.this);
        int dividerHeight = (int) (getResources().getDisplayMetrics().density * 20); // 1dp to pixels
        viewDivider.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider);

        AppCompatButton btnEditCancel=new AppCompatButton(MoreInforService.this);
        btnEditCancel.setText("Cancelar");
        btnEditCancel.layout(50,20,50,20);
        btnEditCancel.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditCancel);

        View viewDivider2 = new View(MoreInforService.this);
        viewDivider2.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider2);

        Button btnEditAccept=new Button(MoreInforService.this);
        btnEditAccept.setText("Aceptar");
        btnEditAccept.setPadding(50,20,50,20);
        btnEditAccept.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditAccept);

        dialog.setView(vertical);
        alertDialog=dialog.create();
        alertDialog.show();

        btnEditCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnEditAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                EditDataService(edtEditName.getText().toString(),edtEditDescription.getText().toString(),edtEditPrice.getText().toString(),uid);
            }
        });
    }

    private void EditDataService(String nombre, String descripcion, String precio, String uid) {

        if (!nombre.isEmpty() && !descripcion.isEmpty() && !precio.isEmpty()){

            String URL2="https://udmyhotelproject.herokuapp.com/myhotel/servicio/"+uid;
            SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

            ProgressDialog pDialog = new ProgressDialog(MoreInforService.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            JSONObject jsonParams = new JSONObject();

            try {
                jsonParams.put("nombre", nombre);
                jsonParams.put("descripcion", descripcion);
                jsonParams.put("precio", Integer.parseInt(precio));
            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.PUT,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                            ManagementActivities.restartActivity(MoreInforService.this);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    Log.e("d",jsonObject.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            )
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

        }else {
            alert(R.layout.my_failed_dialog,"¡Ningun campo puede estar vacio!");
        }

    }

    private void warningAlert(int myLayout, String description, String uid) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(MoreInforService.this);
        View layoutView=getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                StateChange(uid);

            }
        });

    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(MoreInforService.this,myLayout,descriptipn);

    }

    public void StateChange(String uid){
        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/servicio/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(MoreInforService.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.DELETE,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        ManagementActivities.restartActivity(MoreInforService.this);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickHomeAuxiliary(View view){
        ManagementActivities.redirectActivity(this,SessionAuxiliary.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(MoreInforService.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        requestQueue= Volley.newRequestQueue(this);
        drawerLayout=findViewById(R.id.drawer_layout);
        txtNameService=(TextView) findViewById(R.id.txtNameService);
        txtDescriptionService=(TextView) findViewById(R.id.txtDescriptionService);
        txtPriceService=(TextView) findViewById(R.id.txtPriceService);
        txtStateService=(TextView) findViewById(R.id.txtStateService);
        imgService=(ImageView) findViewById(R.id.imgService);
        btnStateChangeService=(Button) findViewById(R.id.btnStateChangeService);
        btnEditService=(Button) findViewById(R.id.btnEditService);
        btnEditImageService=(Button) findViewById(R.id.btnEditImageService);
        img=(ImageView) findViewById(R.id.img);
        image=new ImageView(MoreInforService.this);
        btnEditImageCancel=new AppCompatButton(MoreInforService.this);
        btnEditImageAccept=new Button(MoreInforService.this);
        txtName=(TextView) findViewById(R.id.txtName);
    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}