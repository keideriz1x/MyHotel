package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    Button btnSession;
    CheckedTextView cbRegister,cbForgotPassword;
    CheckBox cbRemember;
    RequestQueue requestQueue;
    TextView txtNameAdministrator;
    TextInputEditText edtEmail,edtPassword;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/auth/login";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeElements();
        SaveData(this);
        validSession();

        btnSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Email=edtEmail.getText().toString().toLowerCase().trim();
                String Password=edtPassword.getText().toString();
                LoadData(this,Email,Password);
                Sesion(Email,Password);
            }
        });

       cbForgotPassword.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Forgot();
           }
       });

        cbRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register();
            }
        });
    }

    public void Sesion(String correo, String clave) {

        JSONObject jsonParams = new JSONObject();

        if (!correo.isEmpty() && !clave.isEmpty()){

            ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("correo", correo);
                jsonParams.put("clave", clave);
            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.POST,
                    URL,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            JSONObject jsonObject= null;

                            try {

                                jsonObject = new JSONObject(response.get("cliente").toString());
                                String Rol=jsonObject.get("rol").toString();

                                SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);
                                SharedPreferences.Editor userAccount = user.edit();
                                userAccount.putString("identificacion",jsonObject.get("identificacion").toString());
                                userAccount.putString("nombre",jsonObject.get("nombre").toString());
                                userAccount.putString("apellido",jsonObject.get("apellido").toString());
                                userAccount.putString("correo",jsonObject.get("correo").toString());
                                userAccount.putString("telefono",jsonObject.get("telefono").toString());
                                userAccount.putString("estado",jsonObject.get("estado").toString());
                                userAccount.putString("rol",jsonObject.get("rol").toString());
                                userAccount.putString("uid",jsonObject.get("uid").toString());
                                userAccount.putString("img",jsonObject.get("img").toString());
                                userAccount.putString("token",response.get("token").toString());
                                userAccount.commit();

                                pDialog.dismiss();

                                if (Rol.equals("ADMIN")){
                                    ManagementActivities.finishActivity(MainActivity.this,SessionAdministrator.class);
                                }else if (Rol.equals("AUX")){
                                    ManagementActivities.finishActivity(MainActivity.this,SessionAuxiliary.class);
                                }else if (Rol.equals("USER")){
                                    ManagementActivities.finishActivity(MainActivity.this,SessionClient.class);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);
                                    if (jsonObject.get("msg").toString().equals("error crendeciales")){

                                        alert(R.layout.my_failed_dialog,"¡Correo y/o contraseña incorrecto!.");

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            );
            requestQueue.add(jsonObjectRequest);
        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void alert(int myLayout, String description) {
        ManagementActivities.showAlertDialog(MainActivity.this,myLayout,description);
    }

    public void Register() {
        ManagementActivities.redirectActivity(MainActivity.this,Register.class);
    }

    public void Forgot(){
        ManagementActivities.redirectActivity(MainActivity.this,ForgotPassword.class);
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        requestQueue= Volley.newRequestQueue(this);
        btnSession=(Button)findViewById(R.id.btnSession);
        cbForgotPassword=(CheckedTextView)findViewById(R.id.cbForgotPassword);
        cbRegister= (CheckedTextView) findViewById(R.id.cbRegister);
        cbRemember= (CheckBox)findViewById(R.id.cbRemember);
        edtEmail= (TextInputEditText) findViewById(R.id.edtEmailAuxiliary);
        edtPassword= (TextInputEditText) findViewById(R.id.edtPassword);
        txtNameAdministrator= (TextView) findViewById(R.id.txtName);
    }

    public void SaveData(Context context) {
        SharedPreferences preferences=getSharedPreferences("Remember",Context.MODE_PRIVATE);
        String sharedEmail=preferences.getString("email","");
        String sharedPassword=preferences.getString("password","");
        boolean stateCheck=preferences.getBoolean("stateCheck",false);
        cbRemember.setChecked(stateCheck);
        edtEmail.setText(sharedEmail);
        edtPassword.setText(sharedPassword);
    }

    public void LoadData(View.OnClickListener context, String Email, String Password){
        SharedPreferences preferences=getSharedPreferences("Remember",Context.MODE_PRIVATE);

        if(cbRemember.isChecked() && !Email.isEmpty() && !Password.isEmpty()){

            SharedPreferences.Editor editor =preferences.edit();
            editor.putString("email", Email);
            editor.putString("password", Password);
            editor.putBoolean("stateCheck", cbRemember.isChecked());
            editor.commit();

        } else {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("email","");
            editor.putString("password", "");
            editor.putBoolean("stateCheck", cbRemember.isChecked());
            editor.commit();
        }
    }

    public void validSession(){
        SharedPreferences preferences=getSharedPreferences("user",Context.MODE_PRIVATE);

        if (preferences.contains("rol")){
            String Rol=preferences.getString("rol","");
            if (Rol.equals("ADMIN")){
                ManagementActivities.finishActivity(MainActivity.this,SessionAdministrator.class);
            }else if (Rol.equals("AUX")){
                ManagementActivities.finishActivity(MainActivity.this,SessionAuxiliary.class);
            }else if (Rol.equals("USER")){
                ManagementActivities.finishActivity(MainActivity.this,SessionClient.class);
            }
        }
    }

}