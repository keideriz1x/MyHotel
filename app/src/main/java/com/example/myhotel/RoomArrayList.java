package com.example.myhotel;

public class RoomArrayList {
    public String numero;
    public String id;
    public String categoria;
    public String ocupado;
    public String uid;

    public RoomArrayList(String numero, String id, String categoria, String ocupado, String uid) {
        this.numero = numero;
        this.id = id;
        this.categoria = categoria;
        this.ocupado = ocupado;
        this.uid = uid;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getOcupado() {
        return ocupado;
    }

    public void setOcupado(String ocupado) {
        this.ocupado = ocupado;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
