package com.example.myhotel;

public class FacturaArrayList {
    public String fecha;
    public String precio;
    public String uid;

    public FacturaArrayList(String fecha, String precio, String uid) {
        this.fecha = fecha;
        this.precio = precio;
        this.uid = uid;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
