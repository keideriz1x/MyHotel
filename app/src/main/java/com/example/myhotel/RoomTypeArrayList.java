package com.example.myhotel;

public class RoomTypeArrayList {
    public String categoria;
    public String camas;
    public String terraza;
    public String img;
    public String uid;
    public int precio;

    public RoomTypeArrayList(String categoria, String camas, String terraza, String img, String uid, int precio) {
        this.categoria = categoria;
        this.camas = camas;
        this.terraza = terraza;
        this.img = img;
        this.uid = uid;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCamas() {
        return camas;
    }

    public void setCamas(String camas) {
        this.camas = camas;
    }

    public String getTerraza() {
        return terraza;
    }

    public void setTerraza(String terraza) {
        this.terraza = terraza;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
