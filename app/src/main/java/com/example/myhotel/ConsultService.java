package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConsultService extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAuxiliaryFragment,NavAdministratorFragment;
    TextView txtName,txtConsultService,tvName,tvPrice,tvSeeMore;
    Button btnSeeMore;
    ImageView img;
    ArrayList<ServiceArrayList> arrayList=new ArrayList<>();
    RequestQueue requestQueue;
    TableRow fila;
    TableLayout tableLayout;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/servicio/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_service);

        InitializeElements();
        ConsultDataService();
        NavLoadData();
        NavFragmentData();

    }

    public void ConsultDataService(){

        ProgressDialog pDialog = new ProgressDialog(ConsultService.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("total");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("servicio").toString());
                                final int total = jsonArray.length();

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());

                                    arrayList.add(new ServiceArrayList(jsonObject.get("nombre").toString(),
                                            jsonObject.get("descripcion").toString(), (Integer) jsonObject.get("precio"),
                                            jsonObject.get("img").toString(),jsonObject.get("estado").toString(),jsonObject.get("uid").toString()));
                                }

                                generateTable(arrayList);

                            }else{

                                txtConsultService.setTextSize(18);
                                txtConsultService.setGravity(Gravity.CENTER);
                                txtConsultService.setText("No se encontro información de servicios.");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public void generateTable(ArrayList<ServiceArrayList> arrayList){
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);
        TableRow.LayoutParams layoutCategory = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);
        TableRow.LayoutParams layoutSeeMore = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(ConsultService.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {

                tvName = new TextView(ConsultService.this);
                tvName.setText("NOMBRE");
                tvName.setTextSize(18);
                tvName.setGravity(Gravity.CENTER);
                tvName.setBackgroundColor(Color.parseColor("#1976D2"));
                tvName.setTextColor(Color.WHITE);
                tvName.setPadding(10, 10, 10, 10);
                tvName.setLayoutParams(layoutName);
                fila.addView(tvName);

                tvPrice = new TextView(ConsultService.this);
                tvPrice.setText("PRECIO");
                tvPrice.setTextSize(18);
                tvPrice.setGravity(Gravity.CENTER);
                tvPrice.setBackgroundColor(Color.parseColor("#1976D2"));
                tvPrice.setTextColor(Color.WHITE);
                tvPrice.setPadding(10, 10, 10, 10);
                tvPrice.setLayoutParams(layoutName);
                fila.addView(tvPrice);

                tvSeeMore = new TextView(ConsultService.this);
                tvSeeMore.setTextSize(18);
                tvSeeMore.setGravity(Gravity.CENTER);
                tvSeeMore.setBackgroundColor(Color.parseColor("#1976D2"));
                tvSeeMore.setTextColor(Color.WHITE);
                tvSeeMore.setPadding(10, 10, 10, 10);
                tvSeeMore.setLayoutParams(layoutName);
                fila.addView(tvSeeMore);

                tableLayout.addView(fila);
            } else {

                tvName = new TextView(ConsultService.this);
                tvName.setTextSize(16);
                tvName.setGravity(Gravity.CENTER);
                tvName.setText(arrayList.get(i).getNombre());
                tvName.setPadding(10, 10, 10, 10);
                tvName.setLayoutParams(layoutName);
                fila.addView(tvName);

                tvPrice = new TextView(ConsultService.this);
                tvPrice.setTextSize(16);
                tvPrice.setGravity(Gravity.CENTER);
                tvPrice.setText(arrayList.get(i).getPrecio()+"");
                tvPrice.setPadding(10, 10, 10, 10);
                tvPrice.setLayoutParams(layoutCategory);
                fila.addView(tvPrice);

                btnSeeMore = new Button(ConsultService.this);
                btnSeeMore.setGravity(Gravity.CENTER);
                btnSeeMore.setBackgroundColor(Color.TRANSPARENT);
                btnSeeMore.setText("Ver mas...");
                btnSeeMore.setTextColor(Color.parseColor("#1976D2"));
                int finalI = i;
                btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ConsultService.this, MoreInforService.class);
                        intent.putExtra("uid", arrayList.get(finalI).getUid());
                        startActivity(intent);
                    }
                });
                btnSeeMore.setPadding(10, 10, 10, 10);
                btnSeeMore.setLayoutParams(layoutSeeMore);
                fila.addView(btnSeeMore);

                tableLayout.addView(fila);

            }
        }
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickHomeAuxiliary(View view){
        ManagementActivities.redirectActivity(this,SessionAuxiliary.class);
    }

    public void ClickConsultService(View view){
        recreate();
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }

    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(ConsultService.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
        }

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        txtConsultService=(TextView)findViewById(R.id.txtConsultService);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        requestQueue= Volley.newRequestQueue(this);
        tableLayout=(TableLayout) findViewById(R.id.tableLayout);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        ManagementActivities.restartActivity(ConsultService.this);
    }

}