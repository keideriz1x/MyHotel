package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SessionClient extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Fragment NavClientFragment;
    TextView txtName,txtRoomList;
    ImageView img;
    ArrayList<RoomTypeArrayList> arrayList=new ArrayList<>();
    RequestQueue requestQueue;
    RecyclerView listRoom;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/tipoH/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_client);

       InitializeElements();
        NavLoadData();
        NavFragmentData();
        ConsultDataRoomType();

    }

    public void ConsultDataRoomType(){

        ProgressDialog pDialog = new ProgressDialog(SessionClient.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("total");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("habitacion").toString());
                                final int total = jsonArray.length();

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    arrayList.add(new RoomTypeArrayList(jsonObject.get("categoria").toString(),
                                            jsonObject.get("camas").toString(), jsonObject.get("terraza").toString(),
                                            jsonObject.get("img").toString(),jsonObject.get("uid").toString(),
                                            (Integer) jsonObject.get("precio")));
                                }

                                generateList(arrayList);


                            }else{

                                txtRoomList.setTextSize(18);
                                txtRoomList.setGravity(Gravity.CENTER);
                                txtRoomList.setText("No hay habitaciones disponibles..");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public void generateList(ArrayList<RoomTypeArrayList> arrayList){

        ListRoomAdapter adapter=new ListRoomAdapter(arrayList);
        SpacingItemDecorator itemDecorator=new SpacingItemDecorator(15);
        listRoom.setAdapter(adapter);
        listRoom.addItemDecoration(itemDecorator);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeClient(View view){
        recreate();
    }

    public void ClickReservation(View view){
        ManagementActivities.redirectActivity(this,Reservation.class);
    }

    public void ClickPayment(View view){
        ManagementActivities.redirectActivity(this,Payment.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(SessionClient.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavClientFragment=new NavClientFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavClientFragment).commit();
    }

    public void NavLoadData(){

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        txtRoomList=(TextView)findViewById(R.id.txtRoomList);
        listRoom=(RecyclerView)findViewById(R.id.listRoom);
        listRoom.setLayoutManager(new LinearLayoutManager(this));
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        requestQueue= Volley.newRequestQueue(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }
}