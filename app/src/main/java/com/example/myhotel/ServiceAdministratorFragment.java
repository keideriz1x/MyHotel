package com.example.myhotel;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ServiceAdministratorFragment extends Fragment {
    View view;
    TextView txtNameAdministrator;

    public ServiceAdministratorFragment() {
        // Required empty public constructor
    }

    public static ServiceAdministratorFragment newInstance(String param1, String param2) {
        ServiceAdministratorFragment fragment = new ServiceAdministratorFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_service_administrator, container, false);

        return view;
    }
}