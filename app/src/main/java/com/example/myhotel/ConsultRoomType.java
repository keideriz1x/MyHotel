package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConsultRoomType extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment,NavAuxiliaryFragment;
    TextView txtName,tvCategoria,tvCamas,tvSeeMore,txtConsulRoomType;
    TableLayout tableLayout;
    RequestQueue requestQueue;
    ArrayList<RoomTypeArrayList> arrayList=new ArrayList<>();
    TableRow fila;
    Button btnSeeMore;
    ImageView img;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/tipoH/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_room_type);
        InitializeElements();
        ConsultDataRoomType();
        NavLoadData();
        NavFragmentData();
    }

    public void ConsultDataRoomType(){

        ProgressDialog pDialog = new ProgressDialog(ConsultRoomType.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("total");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("habitacion").toString());
                                final int total = jsonArray.length();

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    arrayList.add(new RoomTypeArrayList(jsonObject.get("categoria").toString(),
                                            jsonObject.get("camas").toString(), jsonObject.get("terraza").toString(),
                                            jsonObject.get("img").toString(),jsonObject.get("uid").toString(),
                                            (Integer) jsonObject.get("precio")));
                                }

                                generateTable(arrayList);

                            }else{

                                txtConsulRoomType.setTextSize(18);
                                txtConsulRoomType.setGravity(Gravity.CENTER);
                                txtConsulRoomType.setText("No se encontro información de tipos de habitacion.");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public void generateTable(ArrayList<RoomTypeArrayList> arrayList){
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutCategory = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);
        TableRow.LayoutParams layoutBed = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);
        TableRow.LayoutParams layoutSeeMore = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(ConsultRoomType.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvCategoria = new TextView(ConsultRoomType.this);
                tvCategoria.setText("CATEGORIA");
                tvCategoria.setTextSize(18);
                tvCategoria.setGravity(Gravity.CENTER);
                tvCategoria.setBackgroundColor(Color.parseColor("#1976D2"));
                tvCategoria.setTextColor(Color.WHITE);
                tvCategoria.setPadding(10, 10, 10, 10);
                tvCategoria.setLayoutParams(layoutCategory);
                fila.addView(tvCategoria);

                tvCamas = new TextView(ConsultRoomType.this);
                tvCamas.setText("CAMAS");
                tvCamas.setTextSize(18);
                tvCamas.setGravity(Gravity.CENTER);
                tvCamas.setBackgroundColor(Color.parseColor("#1976D2"));
                tvCamas.setTextColor(Color.WHITE);
                tvCamas.setPadding(10, 10, 10, 10);
                tvCamas.setLayoutParams(layoutCategory);
                fila.addView(tvCamas);

                tvSeeMore = new TextView(ConsultRoomType.this);
                tvSeeMore.setTextSize(18);
                tvSeeMore.setGravity(Gravity.CENTER);
                tvSeeMore.setBackgroundColor(Color.parseColor("#1976D2"));
                tvSeeMore.setTextColor(Color.WHITE);
                tvSeeMore.setPadding(10, 10, 10, 10);
                tvSeeMore.setLayoutParams(layoutCategory);
                fila.addView(tvSeeMore);

                tableLayout.addView(fila);
            } else {
                tvCategoria = new TextView(ConsultRoomType.this);
                tvCategoria.setTextSize(16);
                tvCategoria.setGravity(Gravity.CENTER);
                tvCategoria.setText(arrayList.get(i).getCategoria());
                tvCategoria.setPadding(10, 10, 10, 10);
                tvCategoria.setLayoutParams(layoutCategory);
                fila.addView(tvCategoria);

                tvCamas = new TextView(ConsultRoomType.this);
                tvCamas.setTextSize(16);
                tvCamas.setGravity(Gravity.CENTER);
                tvCamas.setText(arrayList.get(i).getCamas());
                tvCamas.setPadding(10, 10, 10, 10);
                tvCamas.setLayoutParams(layoutBed);
                fila.addView(tvCamas);

                btnSeeMore = new Button(ConsultRoomType.this);
                btnSeeMore.setGravity(Gravity.CENTER);
                btnSeeMore.setBackgroundColor(Color.TRANSPARENT);
                btnSeeMore.setText("Ver mas...");
                btnSeeMore.setTextColor(Color.parseColor("#1976D2"));
                int finalI = i;
                btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ConsultRoomType.this, MoreInfoRoomType.class);
                        intent.putExtra("uid", arrayList.get(finalI).getUid());
                        startActivity(intent);
                    }
                });
                btnSeeMore.setPadding(10, 10, 10, 10);
                btnSeeMore.setLayoutParams(layoutSeeMore);
                fila.addView(btnSeeMore);

                tableLayout.addView(fila);

            }
        }
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickConsultRoomType(View view){
        recreate();
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }

    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(ConsultRoomType.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
        }

    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        txtConsulRoomType=(TextView)findViewById(R.id.txtConsulRoomType);
        tableLayout=(TableLayout)findViewById(R.id.tableLayout);
        requestQueue= Volley.newRequestQueue(this);
        img=(ImageView) findViewById(R.id.img);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        ManagementActivities.restartActivity(ConsultRoomType.this);
    }
}