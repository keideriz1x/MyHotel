package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Reservation extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Fragment NavClientFragment;
    TextView txtName,txtConsultReservation,tvRoom,tvFInicio,txtSeeMore;
    ImageView img;
    RequestQueue requestQueue;
    ArrayList<ReservationArrayList> arrayList=new ArrayList<>();
    TableRow fila;
    Button btnSeeMore;
    TableLayout tableLayout;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/reserva/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        InitializeElements();
        ConsultDataReservation();
        NavLoadData();
        NavFragmentData();

    }

    public void ConsultDataReservation(){

        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(Reservation.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            pDialog.dismiss();

                            JSONArray jsonArray= new JSONArray(response.get("servicio").toString());
                            final int total = jsonArray.length();

                            if (total>=1){

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    JSONObject jsonObject2=new JSONObject(jsonObject.get("habitacion").toString());

                                    String[] fechaInicio=jsonObject.get("fechaInicio").toString().split("T");
                                    String[] fechaFin=jsonObject.get("fechaFin").toString().split("T");

                                        arrayList.add(new ReservationArrayList(fechaInicio[0], fechaFin[0],
                                                jsonObject.get("precio").toString(), jsonObject.get("usuario").toString(),
                                                jsonObject2.get("_id").toString(), jsonObject2.get("numero").toString(),
                                                jsonObject.get("estado").toString(),jsonObject.get("uid").toString()));

                                }

                                generateTable(arrayList);

                            }else{

                                txtConsultReservation.setTextSize(18);
                                txtConsultReservation.setGravity(Gravity.CENTER);
                                txtConsultReservation.setText("No se encontro información de reservas.");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(Reservation.this,myLayout,descriptipn);

    }

    private void generateTable(ArrayList<ReservationArrayList> arrayList) {

        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,45);
        TableRow.LayoutParams layoutEmail = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,45);
        TableRow.LayoutParams layoutSeeMore = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,10);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(Reservation.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {
                tvRoom = new TextView(Reservation.this);
                tvRoom.setText("HABITACION");
                tvRoom.setTextSize(18);
                tvRoom.setGravity(Gravity.CENTER);
                tvRoom.setBackgroundColor(Color.parseColor("#1976D2"));
                tvRoom.setTextColor(Color.WHITE);
                tvRoom.setPadding(10, 10, 10, 10);
                tvRoom.setLayoutParams(layoutName);
                fila.addView(tvRoom);

                tvFInicio = new TextView(Reservation.this);
                tvFInicio.setText("FECHA");
                tvFInicio.setTextSize(18);
                tvFInicio.setGravity(Gravity.CENTER);
                tvFInicio.setBackgroundColor(Color.parseColor("#1976D2"));
                tvFInicio.setTextColor(Color.WHITE);
                tvFInicio.setPadding(10, 10, 10, 10);
                tvFInicio.setLayoutParams(layoutName);
                fila.addView(tvFInicio);

                txtSeeMore = new TextView(Reservation.this);
                txtSeeMore.setTextSize(18);
                txtSeeMore.setGravity(Gravity.CENTER);
                txtSeeMore.setBackgroundColor(Color.parseColor("#1976D2"));
                txtSeeMore.setTextColor(Color.WHITE);
                txtSeeMore.setPadding(10, 10, 10, 10);
                txtSeeMore.setLayoutParams(layoutName);
                fila.addView(txtSeeMore);

                tableLayout.addView(fila);
            } else {
                tvRoom = new TextView(Reservation.this);
                tvRoom.setTextSize(16);
                tvRoom.setGravity(Gravity.CENTER);
                tvRoom.setText(arrayList.get(i).getNumeroHabitacion());
                tvRoom.setPadding(10, 10, 10, 10);
                tvRoom.setHorizontallyScrolling(true);
                tvRoom.setEllipsize(TextUtils.TruncateAt.END);
                tvRoom.setMaxLines(1);
                tvRoom.setLayoutParams(layoutName);
                fila.addView(tvRoom);

                tvFInicio = new TextView(Reservation.this);
                tvFInicio.setTextSize(16);
                tvFInicio.setGravity(Gravity.CENTER);
                tvFInicio.setText(arrayList.get(i).getFechaInicio() +" / "+ arrayList.get(i).getFechaFin());
                tvFInicio.setPadding(10, 10, 10, 10);
                tvFInicio.setHorizontallyScrolling(true);
                tvFInicio.setEllipsize(TextUtils.TruncateAt.END);
                tvFInicio.setMaxLines(1);
                tvFInicio.setLayoutParams(layoutEmail);
                fila.addView(tvFInicio);

                btnSeeMore = new Button(Reservation.this);
                btnSeeMore.setGravity(Gravity.CENTER);
                btnSeeMore.setBackgroundColor(Color.TRANSPARENT);
                btnSeeMore.setText("Ver mas...");
                btnSeeMore.setTextColor(Color.parseColor("#1976D2"));
                int finalI = i;
                btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Reservation.this, MoreInfoReservation.class);
                        intent.putExtra("uid", arrayList.get(finalI).getUid());
                        startActivity(intent);
                    }
                });
                btnSeeMore.setPadding(10, 10, 10, 10);
                btnSeeMore.setLayoutParams(layoutSeeMore);
                fila.addView(btnSeeMore);

                tableLayout.addView(fila);

            }
        }

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeClient(View view){
        ManagementActivities.redirectActivity(this,SessionClient.class);
    }

    public void ClickReservation(View view){
        recreate();
    }

    public void ClickPayment(View view){
        ManagementActivities.redirectActivity(this,Payment.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(Reservation.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavClientFragment=new NavClientFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavClientFragment).commit();
    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        txtConsultReservation=(TextView)findViewById(R.id.txtConsultReservation);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        requestQueue= Volley.newRequestQueue(this);
        tableLayout=(TableLayout)findViewById(R.id.tableLayout);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        ManagementActivities.restartActivity(Reservation.this);
    }

}