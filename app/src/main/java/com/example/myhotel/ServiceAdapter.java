package com.example.myhotel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceHolder> {

    ArrayList<ServiceArrayList> listService;
    private String uid;


    public ServiceAdapter(ArrayList<ServiceArrayList> listService) {
        this.listService = listService;
    }

    public void enviarId(String uid) {

        this.uid = uid;

    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_service,null,false);
        return new ServiceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHolder holder, @SuppressLint("RecyclerView") int position) {

        if (!listService.get(position).getImg().equals("")){
            Picasso.get()
                    .load(listService.get(position).getImg())
                    .into(holder.sImage);
        }else{
            Picasso.get()
                    .load(R.drawable.defecto)
                    .into(holder.sImage);
        }

        holder.sTitle.setText(listService.get(position).getNombre());
        holder.sDescription.setText(listService.get(position).getDescripcion());

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String price = formatter.format(listService.get(position).getPrecio());

        holder.sPrice.setText("COP "+price);

        holder.sService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningAlert(R.layout.my_warning_dialog,"¿Esta seguro de agregar servicio?",uid,listService.get(position).getUid(),v.getContext());
            }
        });

    }

    private void AddDataService(String uid,String id,Context context) {

            String URL2="https://udmyhotelproject.herokuapp.com/myhotel/gasto/"+uid;
            SharedPreferences user = context.getSharedPreferences("user", Context.MODE_PRIVATE);

            RequestQueue requestQueue= Volley.newRequestQueue(context);

            ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(context.getDrawable(R.drawable.logohotel));
            pDialog.show();

            JSONObject jsonParams = new JSONObject();

            try {
                jsonParams.put("servicio", id);

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.POST,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();

                            alert(R.layout.my_success_dialog,"¡Servicio agregado exitosamente!",context);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(jsonError);

                                    Log.e("d",jsonObject.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
            )
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

    }

    private void alert(int myLayout, String description, Context context) {
        ManagementActivities.showAlertDialog((Activity) context,myLayout,description);
    }

    private void warningAlert(int myLayout, String description, String uid,String id,Context context) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(context);
        Activity activity= (Activity) context;
        View layoutView=activity.getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                AddDataService(uid,id,context);

            }
        });

    }

    @Override
    public int getItemCount() {
        return listService.size();
    }

    public class ServiceHolder extends RecyclerView.ViewHolder {

        ImageView sImage;
        TextView sTitle,sDescription,sPrice;
        Button sService;

        public ServiceHolder(@NonNull View itemView) {
            super(itemView);

            sImage=itemView.findViewById(R.id.sImage);
            sTitle=itemView.findViewById(R.id.sTitle);
            sDescription=itemView.findViewById(R.id.sDescription);
            sPrice=itemView.findViewById(R.id.sPrice);
            sService=itemView.findViewById(R.id.sService);

        }
    }
}
