package com.example.myhotel;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MoreInfoRoom extends AppCompatActivity {

    DrawerLayout drawerLayout;
    RequestQueue requestQueue;
    Fragment NavAdministratorFragment,NavAuxiliaryFragment;
    TextView txtName,txtNumberRoom,txtStateRoom,txtTypeRoom;
    ImageView img,imgRoom;
    Button btnStateChangeRoom,btnEditRoom;
    ArrayList<RoomTypeArrayList> arrayList=new ArrayList<>();
    String tipo_habitacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_infor_room);

        InitializeElements();

        String uid = getIntent().getExtras().getString("uid");
        String URL="https://udmyhotelproject.herokuapp.com/myhotel/habitacion/"+uid;

        ConsultDataRoom(URL,uid,ConsultDataRoomType());

        NavLoadData();
        NavFragmentData();

    }

    public void ConsultDataRoom(String URL, String uid, ArrayList<RoomTypeArrayList> roomTypeArrayLists){

        ProgressDialog pDialog = new ProgressDialog(MoreInfoRoom.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {
                            JSONObject jsonObject= new JSONObject(response.get("habitacion").toString());
                            JSONObject jsonObject2=new JSONObject(jsonObject.get("tipo_habitacion").toString());

                            if (!jsonObject2.get("img").equals("")) {
                                Picasso.get()
                                        .load(jsonObject2.get("img").toString())
                                        .into(imgRoom);
                            }else{
                                Picasso.get()
                                        .load(R.drawable.defecto)
                                        .into(imgRoom);
                            }

                            txtNumberRoom.setText("Habitacion "+jsonObject.get("numero").toString());
                            txtTypeRoom.setText("Tipo de habitación: "+jsonObject2.get("categoria").toString());

                            if (jsonObject.get("ocupado").toString().equals("true")){
                                txtStateRoom.setText("Estado: Ocupado.");
                            }else{
                                txtStateRoom.setText("Estado: Disponible.");
                            }

                            btnStateChangeRoom.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    warningAlert(R.layout.my_warning_dialog,"¿Esta seguro de cambiar el estado?",uid);
                                }
                            });

                            btnEditRoom.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        EditRoom(jsonObject.get("numero").toString(),jsonObject2.get("categoria").toString(),uid,roomTypeArrayLists);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public ArrayList<RoomTypeArrayList> ConsultDataRoomType(){

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/tipoH/?limite=100";

        ProgressDialog pDialog = new ProgressDialog(MoreInfoRoom.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("total");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("habitacion").toString());
                                final int total = jsonArray.length();

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    arrayList.add(new RoomTypeArrayList(jsonObject.get("categoria").toString(),
                                            jsonObject.get("camas").toString(), jsonObject.get("terraza").toString(),
                                            jsonObject.get("img").toString(),jsonObject.get("uid").toString(),
                                            (Integer) jsonObject.get("precio")));
                                }

                            }else{



                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);

        return arrayList;
    }

    private void EditRoom(String numero, String categoria, String uid, ArrayList<RoomTypeArrayList> roomTypeArrayLists) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MoreInfoRoom.this);
        AlertDialog alertDialog;

        LinearLayout vertical=new LinearLayout(MoreInfoRoom.this);
        vertical.setOrientation(LinearLayout.VERTICAL);
        vertical.setPadding(50,30,50,50);

        TextView txtEditTitle=new TextView(MoreInfoRoom.this);
        txtEditTitle.setText("Editar habitación");
        txtEditTitle.setTextSize(27);
        txtEditTitle.setGravity(Gravity.CENTER);
        txtEditTitle.setTypeface(null, Typeface.BOLD);
        txtEditTitle.setPadding(20,20,20,20);
        vertical.addView(txtEditTitle);

        TextInputLayout NumberTextInputLayout = new TextInputLayout(MoreInfoRoom.this);
        NumberTextInputLayout.setHint("Numero de habitación");
        TextInputEditText edtEditNumber = new TextInputEditText(NumberTextInputLayout.getContext());
        edtEditNumber.setText(numero);
        edtEditNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtEditNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(4)});
        NumberTextInputLayout.addView(edtEditNumber);
        NumberTextInputLayout.setPadding(0,20,0,20);
        vertical.addView(NumberTextInputLayout);

        Spinner spRoom= new Spinner(MoreInfoRoom.this);
        spRoom.setPadding(0,20,0,20);
        ArrayAdapter<RoomTypeArrayList> arrayAdapter=new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,roomTypeArrayLists);

        spRoom.setAdapter(arrayAdapter);

        for(int i=0; i < roomTypeArrayLists.size(); i++)
            if(roomTypeArrayLists.get(i).getCategoria().equals(categoria)){
                spRoom.setSelection(i);
            }


        spRoom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                RoomTypeArrayList roomTypeArrayList=(RoomTypeArrayList) parent.getItemAtPosition(position);
                tipo_habitacion=roomTypeArrayList.getUid();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        vertical.addView(spRoom);

        View viewDivider = new View(MoreInfoRoom.this);
        int dividerHeight = (int) (getResources().getDisplayMetrics().density * 20); // 1dp to pixels
        viewDivider.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider);

        AppCompatButton btnEditCancel=new AppCompatButton(MoreInfoRoom.this);
        btnEditCancel.setText("Cancelar");
        btnEditCancel.layout(50,20,50,20);
        btnEditCancel.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditCancel);

        View viewDivider2 = new View(MoreInfoRoom.this);
        viewDivider2.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dividerHeight));
        vertical.addView(viewDivider2);

        Button btnEditAccept=new Button(MoreInfoRoom.this);
        btnEditAccept.setText("Aceptar");
        btnEditAccept.setPadding(50,20,50,20);
        btnEditAccept.setBackgroundResource(R.drawable.button_background_warning);
        vertical.addView(btnEditAccept);

        dialog.setView(vertical);
        alertDialog=dialog.create();
        alertDialog.show();

        btnEditCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnEditAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                EditDataRoomType(edtEditNumber.getText().toString(),tipo_habitacion,uid);
            }
        });
    }

    private void EditDataRoomType(String number, String tipo_habitacion, String uid) {

        JSONObject jsonParams = new JSONObject();

        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/habitacion/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if ((!number.isEmpty()) && (!tipo_habitacion.isEmpty())){

            ProgressDialog pDialog = new ProgressDialog(MoreInfoRoom.this);
            pDialog.setMessage("Cargando...");
            pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
            pDialog.show();

            try {
                jsonParams.put("numero", Integer.parseInt(number));
                jsonParams.put("tipo_habitacion", tipo_habitacion);

            } catch ( JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                    Request.Method.PUT,
                    URL2,
                    jsonParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.dismiss();


                            ManagementActivities.restartActivity(MoreInfoRoom.this);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();

                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);

                                Log.e("error",jsonError);

                            }
                        }
                    }
            ){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    params.put("x-token", user.getString("token", ""));

                    return params;
                }
            };
            requestQueue.add(jsonObjectRequest);

        }else{

            alert(R.layout.my_failed_dialog,"¡Ningún campo puede estar vacio!.");
        }

    }

    private void warningAlert(int myLayout, String description, String uid) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(MoreInfoRoom.this);
        View layoutView=getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                StateChange(uid);

            }
        });

    }

    public void StateChange(String uid){
        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/habitacion/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(MoreInfoRoom.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.DELETE,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        ManagementActivities.restartActivity(MoreInfoRoom.this);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(MoreInfoRoom.this,myLayout,descriptipn);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickHomeAuxiliary(View view){
        ManagementActivities.redirectActivity(this,SessionAuxiliary.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(MoreInfoRoom.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
        }

    }

    private void InitializeElements() {
        getSupportActionBar().hide();
        requestQueue= Volley.newRequestQueue(this);
        drawerLayout=findViewById(R.id.drawer_layout);
        txtName=(TextView) findViewById(R.id.txtName);
        txtNumberRoom=(TextView) findViewById(R.id.txtNumberRoom);
        txtStateRoom=(TextView) findViewById(R.id.txtStateRoom);
        txtTypeRoom=(TextView) findViewById(R.id.txtTypeRoom);
        btnStateChangeRoom=(Button)findViewById(R.id.btnStateChangeRoom);
        btnEditRoom=(Button)findViewById(R.id.btnEditRoom);
        img=(ImageView) findViewById(R.id.img);
        imgRoom=(ImageView) findViewById(R.id.imgRoom);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}