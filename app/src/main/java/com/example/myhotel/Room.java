package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

public class Room extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment,RoomAdministratorFragment,RoomAuxiliaryFragment,NavAuxiliaryFragment;
    TextView txtName;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        InitializeElements();
        NavLoadData();
        NavFragmentData();
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickCreateRoom(View view){
        ManagementActivities.redirectActivity(this,CreateRoom.class);
    }

    public void ClickCreateRoomType(View view){
        ManagementActivities.redirectActivity(this,CreateRoomType.class);
    }

    public void ClickConsultRoomType(View view){
        ManagementActivities.redirectActivity(this,ConsultRoomType.class);
    }

    public void ClickConsultRoom(View view){
        ManagementActivities.redirectActivity(this,ConsultRoom.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickHomeAuxiliary(View view){
        ManagementActivities.redirectActivity(this,SessionAuxiliary.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }

    public void ClickRoom(View view){
        recreate();
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(Room.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        RoomAdministratorFragment=new RoomAdministratorFragment();
        RoomAuxiliaryFragment=new RoomAuxiliaryFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentRoomContainer,RoomAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentRoomContainer,RoomAuxiliaryFragment).commit();
        }

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}