package com.example.myhotel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ConsultRoom extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAuxiliaryFragment,NavAdministratorFragment;
    TextView txtName,txtConsultRoom,tvCategoria,tvNumero,tvSeeMore;
    Button btnSeeMore;
    ImageView img;
    RequestQueue requestQueue;
    ArrayList<RoomArrayList> arrayList=new ArrayList<>();
    TableRow fila;
    TableLayout tableLayout;

    private static final String URL="https://udmyhotelproject.herokuapp.com/myhotel/habitacion/?limite=100";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_room);
        InitializeElements();
        ConsultDataRoom();
        NavLoadData();
        NavFragmentData();

    }

    public void ConsultDataRoom(){

        ProgressDialog pDialog = new ProgressDialog(ConsultRoom.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int Row= (int) response.get("total");

                            pDialog.dismiss();

                            if (Row>=1){
                                JSONArray jsonArray= new JSONArray(response.get("habitacion").toString());
                                final int total = jsonArray.length();

                                for (int i=0;i<total;i++){
                                    JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                    JSONObject jsonObject2=new JSONObject(jsonObject.get("tipo_habitacion").toString());

                                    arrayList.add(new RoomArrayList(jsonObject.get("numero").toString(),
                                            jsonObject2.get("_id").toString(), jsonObject2.get("categoria").toString(),
                                            jsonObject.get("ocupado").toString(),jsonObject.get("uid").toString()));
                                }

                                generateTable(arrayList);

                            }else{

                                txtConsultRoom.setTextSize(18);
                                txtConsultRoom.setGravity(Gravity.CENTER);
                                txtConsultRoom.setText("No se encontro información de habitaciones.");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public void generateTable(ArrayList<RoomArrayList> arrayList){
        TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams layoutNumber = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);
        TableRow.LayoutParams layoutCategory = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);
        TableRow.LayoutParams layoutSeeMore = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,33);

        for(int i = -1 ; i < arrayList.size() ; i++) {
            fila = new TableRow(ConsultRoom.this);
            fila.setLayoutParams(layoutRow);

            if(i == -1) {

                tvNumero = new TextView(ConsultRoom.this);
                tvNumero.setText("HABITACION");
                tvNumero.setTextSize(18);
                tvNumero.setGravity(Gravity.CENTER);
                tvNumero.setBackgroundColor(Color.parseColor("#1976D2"));
                tvNumero.setTextColor(Color.WHITE);
                tvNumero.setPadding(10, 10, 10, 10);
                tvNumero.setLayoutParams(layoutNumber);
                fila.addView(tvNumero);

                tvCategoria = new TextView(ConsultRoom.this);
                tvCategoria.setText("CATEGORIA");
                tvCategoria.setTextSize(18);
                tvCategoria.setGravity(Gravity.CENTER);
                tvCategoria.setBackgroundColor(Color.parseColor("#1976D2"));
                tvCategoria.setTextColor(Color.WHITE);
                tvCategoria.setPadding(10, 10, 10, 10);
                tvCategoria.setLayoutParams(layoutNumber);
                fila.addView(tvCategoria);

                tvSeeMore = new TextView(ConsultRoom.this);
                tvSeeMore.setTextSize(18);
                tvSeeMore.setGravity(Gravity.CENTER);
                tvSeeMore.setBackgroundColor(Color.parseColor("#1976D2"));
                tvSeeMore.setTextColor(Color.WHITE);
                tvSeeMore.setPadding(10, 10, 10, 10);
                tvSeeMore.setLayoutParams(layoutNumber);
                fila.addView(tvSeeMore);

                tableLayout.addView(fila);
            } else {

                tvNumero = new TextView(ConsultRoom.this);
                tvNumero.setTextSize(16);
                tvNumero.setGravity(Gravity.CENTER);
                tvNumero.setText(arrayList.get(i).getNumero());
                tvNumero.setPadding(10, 10, 10, 10);
                tvNumero.setLayoutParams(layoutNumber);
                fila.addView(tvNumero);

                tvCategoria = new TextView(ConsultRoom.this);
                tvCategoria.setTextSize(16);
                tvCategoria.setGravity(Gravity.CENTER);
                tvCategoria.setText(arrayList.get(i).getCategoria());
                tvCategoria.setPadding(10, 10, 10, 10);
                tvCategoria.setLayoutParams(layoutCategory);
                fila.addView(tvCategoria);

                btnSeeMore = new Button(ConsultRoom.this);
                btnSeeMore.setGravity(Gravity.CENTER);
                btnSeeMore.setBackgroundColor(Color.TRANSPARENT);
                btnSeeMore.setText("Ver mas...");
                btnSeeMore.setTextColor(Color.parseColor("#1976D2"));
                int finalI = i;
                btnSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ConsultRoom.this, MoreInfoRoom.class);
                        intent.putExtra("uid", arrayList.get(finalI).getUid());
                        startActivity(intent);
                    }
                });
                btnSeeMore.setPadding(10, 10, 10, 10);
                btnSeeMore.setLayoutParams(layoutSeeMore);
                fila.addView(btnSeeMore);

                tableLayout.addView(fila);

            }
        }
    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAuxiliary(View view){
        ManagementActivities.redirectActivity(this,SessionAuxiliary.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickConsultRoom(View view){
        recreate();
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }

    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(ConsultRoom.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        NavAuxiliaryFragment=new NavAuxiliaryFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        String rol=user.getString("rol","");

        if (rol.equals("ADMIN")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();
        }else if (rol.equals("AUX")){
            getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAuxiliaryFragment).commit();
        }

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        txtConsultRoom=(TextView)findViewById(R.id.txtConsultRoom);
        drawerLayout=findViewById(R.id.drawer_layout);
        img=(ImageView) findViewById(R.id.img);
        requestQueue= Volley.newRequestQueue(this);
        tableLayout=(TableLayout) findViewById(R.id.tableLayout);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}