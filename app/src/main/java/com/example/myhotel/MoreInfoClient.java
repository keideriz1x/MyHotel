package com.example.myhotel;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MoreInfoClient extends AppCompatActivity {

    DrawerLayout drawerLayout;
    Fragment NavAdministratorFragment;
    TextView txtName,txtNameClient,txtEmailClient,txtStateClient,txtPhoneClient;
    RequestQueue requestQueue;
    ImageView img,imgClient;
    Button btnStateChangeClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info_client);

        InitializeElements();

        String uid = getIntent().getExtras().getString("uid");
        String URL="https://udmyhotelproject.herokuapp.com/myhotel/usuario/"+uid;

        ConsultDataClient(URL,uid);

        NavLoadData();
        NavFragmentData();

    }

    private void ConsultDataClient(String URL, String uid) {

        ProgressDialog pDialog = new ProgressDialog(MoreInfoClient.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        try {
                            JSONObject jsonObject= new JSONObject(response.get("resp").toString());

                            if (!jsonObject.get("img").equals("")) {
                                Picasso.get()
                                        .load(jsonObject.get("img").toString())
                                        .into(imgClient);
                            }else{
                                Picasso.get()
                                        .load(R.drawable.defecto)
                                        .into(imgClient);
                            }

                            txtNameClient.setText("Cliente "+jsonObject.get("nombre").toString()+" "+jsonObject.get("apellido").toString());
                            txtEmailClient.setText("Correo: "+jsonObject.get("correo").toString());

                            if (jsonObject.get("estado").toString().equals("true")){
                                txtStateClient.setText("Estado: Habilitado.");
                            }else{
                                txtStateClient.setText("Estado: Deshabilitado.");
                            }

                            if (!jsonObject.get("telefono").toString().isEmpty()){
                                txtPhoneClient.setText(jsonObject.get("telefono").toString());
                            }

                            btnStateChangeClient.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    warningAlert(R.layout.my_warning_dialog,"¿Esta seguro de cambiar el estado?",uid);
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void warningAlert(int myLayout, String description, String uid) {

        AlertDialog.Builder builderDialog;
        AlertDialog alertDialog;
        builderDialog=new AlertDialog.Builder(MoreInfoClient.this);
        View layoutView=getLayoutInflater().inflate(myLayout,null);

        AppCompatButton dialogButton=layoutView.findViewById(R.id.btnCancel);
        AppCompatButton dialogButton2=layoutView.findViewById(R.id.btnAccept);
        ((TextView)layoutView.findViewById(R.id.txt_success)).setText(description);
        builderDialog.setView(layoutView);
        alertDialog=builderDialog.create();
        alertDialog.show();

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                StateChange(uid);

            }
        });

    }

    public void StateChange(String uid){
        String URL2="https://udmyhotelproject.herokuapp.com/myhotel/usuario/"+uid;
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        ProgressDialog pDialog = new ProgressDialog(MoreInfoClient.this);
        pDialog.setMessage("Cargando...");
        pDialog.setIndeterminateDrawable(getDrawable(R.drawable.logohotel));
        pDialog.show();

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.DELETE,
                URL2,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();

                        ManagementActivities.restartActivity(MoreInfoClient.this);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonError);

                                if (jsonObject.get("msg").toString().equals("error")){
                                    alert(R.layout.my_failed_dialog,jsonObject.get("description").toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-token", user.getString("token", ""));

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }

    private void alert(int myLayout, String descriptipn) {

        ManagementActivities.showAlertDialog(MoreInfoClient.this,myLayout,descriptipn);

    }

    public void ClickMenu(View view){
        ManagementActivities.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view){
        ManagementActivities.redirectActivity(this,EditProfile.class);
    }

    public void ClickHomeAdministrator(View view){
        ManagementActivities.redirectActivity(this,SessionAdministrator.class);
    }

    public void ClickUser(View view){
        ManagementActivities.redirectActivity(this,User.class);
    }


    public void ClickRoom(View view){
        ManagementActivities.redirectActivity(this,Room.class);
    }

    public void ClickService(View view){
        ManagementActivities.redirectActivity(this,Service.class);
    }

    public void ClickLogout(View view){

        ManagementActivities.logout(MoreInfoClient.this,R.layout.my_warning_dialog,"¿Esta seguro de cerrar sesión?");

    }

    public void NavLoadData(){
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (!user.getString("img","").equals("")) {
            Picasso.get()
                    .load(user.getString("img",""))
                    .transform(new CircleTransform())
                    .into(img);
        }else{
            Picasso.get()
                    .load(R.drawable.profile)
                    .transform(new CircleTransform())
                    .into(img);
        }

        txtName.setText(user.getString("nombre", "")+" "+user.getString("apellido", ""));
    }

    public void NavFragmentData(){
        NavAdministratorFragment=new NavAdministratorFragment();
        SharedPreferences user = getSharedPreferences("user", Context.MODE_PRIVATE);

        getSupportFragmentManager().beginTransaction().add(R.id.FragmentNavContainer,NavAdministratorFragment).commit();

    }

    public void InitializeElements(){
        getSupportActionBar().hide();
        txtName=(TextView)findViewById(R.id.txtName);
        txtNameClient=(TextView)findViewById(R.id.txtNameClient);
        txtEmailClient=(TextView)findViewById(R.id.txtEmailClient);
        txtStateClient=(TextView)findViewById(R.id.txtStateClient);
        txtPhoneClient=(TextView)findViewById(R.id.txtPhoneClient);
        drawerLayout=findViewById(R.id.drawer_layout);
        requestQueue= Volley.newRequestQueue(this);
        img=(ImageView) findViewById(R.id.img);
        imgClient=(ImageView) findViewById(R.id.imgClient);
        btnStateChangeClient=(Button) findViewById(R.id.btnStateChangeClient);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ManagementActivities.closeDrawer(drawerLayout);
    }

}